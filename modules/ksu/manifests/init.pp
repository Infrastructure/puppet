class ksu {

        file { ".k5login":
                path    => "/root/.k5login",
                mode    => 660,
                owner   => root,
                group   => root,
                ensure  => present,
                source  => "puppet:///modules/ksu/k5login",
        }
}
