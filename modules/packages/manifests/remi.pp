class packages::remi {

    file { 'remi.repo':
            ensure  => present,
            path    => '/etc/yum.repos.d/remi.repo',
            mode    => '0644',
            owner   => 'root',
            group   => 'root',
            content => $::operatingsystemrelease ? {
                /^6\./ => template('packages/remi-6.repo'),
                /^7\./ => template('packages/remi-7.repo'),
            }
    }

    file { 'RPM-GPG-KEY-remi':
            ensure => present,
            path   => '/etc/pki/rpm-gpg/RPM-GPG-KEY-remi',
            mode   => '0644',
            owner  => root,
            group  => root,
            source => 'puppet:///modules/packages/RPM-GPG-KEY-remi'
    }
}
