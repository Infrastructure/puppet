class packages::autoupdate {
    package { 'yum-cron':
            ensure => present,
    }

    service { 'yum-cron':
            ensure     => running,
            enable     => true,
            hasrestart => true,
            hasstatus  => true,
            require    => Package['yum-cron']
    }

    if ($::operatingsystem == 'RedHat' or $::operatingsystem == 'CentOS') and $::lsbmajdistrelease >= '7' {
            
            augeas { 'apply_updates':
                    context => '/files/etc/yum/yum-cron.conf',
                    changes => [
                            'set commands/apply_updates yes',
                            'set commands/update_messages yes',
                            'set commands/download_updates yes'
                    ],
                    notify  => Service['yum-cron'],
            }
    }
}
