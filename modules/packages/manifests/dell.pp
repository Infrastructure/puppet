class packages::dell {

    file { 'dell-omsa-repository.repo':
            ensure   => present,
            path     => '/etc/yum.repos.d/dell-omsa-repository.repo',
            mode     => '0644',
            owner    => 'root',
            group    => 'root',
            source => 'puppet:///modules/packages/dell-omsa-repository.repo',
    }

    file { 'RPM-GPG-KEY-dell':
            ensure => present,
            path   => '/etc/pki/rpm-gpg/RPM-GPG-KEY-dell',
            mode   => '0644',
            owner  => root,
            group  => root,
            source => 'puppet:///modules/packages/RPM-GPG-KEY-dell',
    }

    file { 'RPM-GPG-KEY-dell2':
            ensure => present,
            path   => '/etc/pki/rpm-gpg/RPM-GPG-KEY-dell2',
            mode   => '0644',
            owner  => root,
            group  => root,
            source => 'puppet:///modules/packages/RPM-GPG-KEY-dell2',
    }

    file { 'RPM-GPG-KEY-libsmbios':
            ensure => present,
            path   => '/etc/pki/rpm-gpg/RPM-GPG-KEY-libsmbios',
            mode   => '0644',
            owner  => root,
            group  => root,
            source => 'puppet:///modules/packages/RPM-GPG-KEY-libsmbios',
    }

}
