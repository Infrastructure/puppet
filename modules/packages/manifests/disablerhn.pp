class packages::disablerhn {

        package { 'rhn-virtualization-host':
                ensure => absent,
        }
        
        file { '/etc/sysconfig/rhn/systemid':
                ensure  => absent,
        }
        
        augeas { "disable_yum_rhn_plugin":
                context => "/files/etc/yum/pluginconf.d/rhnplugin.conf/main",
                changes => [
                        "set enabled 0",
                        ],
        }
}
