class packages::gnome {
    file { '/etc/yum.repos.d/gnome.repo':
            ensure  => absent,
    }

    file { 'RPM-GPG-KEY-gnome':
            ensure => present,
            path   => '/etc/pki/rpm-gpg/RPM-GPG-KEY-gnome',
            mode   => '0644',
            owner  => root,
            group  => root,
            source => 'puppet:///modules/packages/RPM-GPG-KEY-gnome'
    }
}
