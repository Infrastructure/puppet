class packages::epel {

    file { 'epel.repo':
            ensure   => present,
            path     => '/etc/yum.repos.d/epel.repo',
            mode     => '0644',
            owner    => 'root',
            group    => 'root',
            content  => template("packages/epel-${::lsbmajdistrelease}.repo.erb"),
            require  => Package['redhat-lsb'],
    }

    file { 'epel.testing':
            path   => '/etc/yum.repos.d/epel-testing.repo',
            ensure => absent,
    }

    package { 'epel-release':
            ensure  => present,
            require => File['/etc/yum.repos.d/epel.repo'];
    }
}
