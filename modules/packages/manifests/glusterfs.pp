class packages::glusterfs {

    if ($::operatingsystem == "RedHat" or $::operatingsystem == "CentOS") and (versioncmp($::operatingsystemrelease, '7') >= 0) {
        sm_add_repo { 'Enable rh-gluster-3-for-rhel-7-server-rpms':
            repo_name => 'rh-gluster-3-for-rhel-7-server-rpms'
        }
    } else {
        sm_add_repo { 'Enable rhs-3-for-rhel-6-server-rpms':
            repo_name => 'rhs-3-for-rhel-6-server-rpms'
        }
    }
}
