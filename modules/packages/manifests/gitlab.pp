class packages::gitlab {

    file { 'gitlab.repo':
            ensure   => present,
            path     => '/etc/yum.repos.d/gitlab.repo',
            mode     => '0644',
            owner    => 'root',
            group    => 'root',
            content  => template("packages/gitlab.repo.erb"),
            require  => Package['redhat-lsb'],
    }
}
