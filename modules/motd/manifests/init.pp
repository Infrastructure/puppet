class motd {
        file { '/etc/motd':
                ensure  => file,
                content => template('motd/base.erb', "motd/system/${::fqdn}.erb"),
        }
}
