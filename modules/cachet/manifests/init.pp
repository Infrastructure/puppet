class cachet (
  $app_key,
  $db_username,
  $db_password,

  $app_url       = 'https://status.gnome.org',
  $db_driver     = 'mysql',
  $db_host       = '127.0.0.1',
  $db_name       = 'cachet',
  $db_port       = '3306',
  $image_version = '2.3.18-6',
) {
  include ::podman::install
  include ::cachet::systemctl

  file { '/etc/cachet.conf':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0640',
    content => template('cachet/cachet.conf.erb'),
  }

  file { '/etc/systemd/system/cachet.service':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('cachet/cachet.service.erb'),
    notify  => Exec['/bin/systemctl daemon-reload'],
  }

  service { 'cachet':
    ensure    => true,
    enable    => true,
    require   => [ File['/etc/systemd/system/cachet.service'], File['/etc/cachet.conf'] ],
    subscribe => Exec['/bin/systemctl daemon-reload'],
  }

  file { '/usr/local/bin/cachet_cleanup.py':
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => 'puppet:///modules/cachet/cachet_cleanup.py',
  }

  # cron { 'cachet_cleanup':
  #   command => '/usr/local/bin/cachet_cleanup.py',
  #   user    => root,
  #   hour    => [0],
  #   minute  => [0],
  #   require => File['/usr/local/bin/cachet_cleanup.py'],
  # }
}
