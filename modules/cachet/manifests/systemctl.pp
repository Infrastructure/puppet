class cachet::systemctl {
  exec { '/bin/systemctl daemon-reload':
      refreshonly => true,
    }
}
