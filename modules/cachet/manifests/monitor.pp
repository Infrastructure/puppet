class cachet::monitor (
  $token,
  $image_version = '3.8.0-4',
) {
  include ::podman::install
  include ::cachet::systemctl

  file { '/etc/cachet-monitor.conf':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('cachet/cachet-monitor.conf.erb'),
  }

  file { '/home/admin/secret/cachet_token':
    ensure  => present,
    mode    => '0644',
    owner   => root,
    group   => root,
    content => template('cachet/cachet_token.erb'),
    require => File['/home/admin/secret'],
  }

  file { '/etc/systemd/system/cachet-monitor.service':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('cachet/cachet-monitor.service.erb'),
    notify  => Exec['/bin/systemctl daemon-reload'],
  }

  service { 'cachet-monitor':
    ensure    => true,
    enable    => true,
    require   => [ File['/etc/systemd/system/cachet-monitor.service'], File['/etc/cachet-monitor.conf'] ],
    subscribe => Exec['/bin/systemctl daemon-reload'],
  }
}
