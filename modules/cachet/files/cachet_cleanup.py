#!/usr/bin/env python

import requests
import os

import datetime
from dateutil.parser import parse as dateparser


class Cachet(object):
    def __init__(self, token, base_url="https://status.gnome.org"):
        self.url = "{}/api/v1".format(base_url)
        self.headers = {"X-Cachet-Token": token}

    def get_incidents(self):
        url = "{}/incidents?per_page=100".format(self.url)
        resp = requests.get(url, headers=self.headers)
        total_pages = resp.json()["meta"]["pagination"]["total_pages"]
        incidents = []

        for page in range(1, total_pages + 1):
            url = "{}/incidents?per_page=100?page={}".format(self.url, page)
            resp = requests.get(url, headers=self.headers)
            incidents.extend(resp.json()["data"])

        return incidents

    def delete_incident(self, id):
        url = "{}/incidents/{}".format(self.url, id)
        resp = requests.delete(url, headers=self.headers)
        if resp.status_code == 200:
            return True
        else:
            return False


if __name__ == "__main__":
    CACHET_TOKEN = os.getenv("CACHET_TOKEN")
    if CACHET_TOKEN is None:
        with open("/home/admin/secret/cachet_token") as f:
            tokenfile = f.readline()
        CACHET_TOKEN = tokenfile.rstrip().split("=")[1]

    cachet = Cachet(CACHET_TOKEN)
    incidents = cachet.get_incidents()
    timedelta = datetime.datetime.now() - datetime.timedelta(days=7)

    for incident in incidents:
        if incident["human_status"] == "Fixed" and incident["visible"] != 1:
            if timedelta > dateparser(incident["updated_at"]):
                cachet.delete_incident(incident["id"])
