# Class to manage ACLs

class acl {

    package { 'acl':
        ensure => installed,
    }

    package { 'gawk':
        ensure => installed,
    }

    include acl::gdefault
    include acl::udefault
    include acl::user
    include acl::group
}
