class acl::udefault {
  define acl_udefault( $id, $mode, $path) {
          exec { $name :
                  command => "/usr/bin/setfacl -m d:u:${id}:${mode} ${path}",
                  onlyif  => "/usr/bin/getfacl ${path}  2>&1 | awk -F: ' \$1 ~/^default/ && \$2 ~ /user/ && \$3 ~/${id}/  && \$4 ~ /${mode}/  { exit 1 } '  "
          }
  }
}
