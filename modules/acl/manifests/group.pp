class acl::group {
  define acl_group( $id, $mode, $path) {
          exec { $name :
                  command => "/usr/bin/setfacl -m g:${id}:${mode} ${path}",
                  onlyif  => "/usr/bin/getfacl ${path} 2>&1 | awk -F: ' \$1 ~ /^group/ && \$2 ~/${id}/  && \$3 ~ /${mode}/  { exit 1 } '  "
          }
  }
}
