class acl::gdefault {
  define acl_gdefault( $id, $mode, $path) {
        exec { $name :
                command => "/usr/bin/setfacl -m d:g:${id}:${mode} ${path}",
                onlyif  => "/usr/bin/getfacl ${path}  2>&1 | awk -F: ' \$1 ~ /^default/ && \$2 ~ /group/ && \$3 ~/${id}/  && \$4 ~ /${mode}/  { exit 1 } '  "
        }
  }
}
