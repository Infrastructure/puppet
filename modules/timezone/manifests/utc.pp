# The default UTC timezone class
class timezone::utc {
    $timezone = 'UTC'

    include timezone
}
