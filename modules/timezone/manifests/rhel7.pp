# A timezone class for RHEL 7 hosts
class timezone::rhel7 {

        include selinux::timedatectl_puppet

        exec { 'set-timezone':
            command => '/usr/bin/timedatectl set-timezone UTC',
            unless  => "/usr/bin/test `timedatectl | /usr/bin/grep 'Time zone' | /usr/bin/awk '{print \$3}'` = 'UTC'",
        }
}
