# Puppet class to manage timezones, UTC is the default
# TZ that should be applied on every machine.
class timezone {
        
        $file_path = $::operatingsystem ? {
                debian  => '/etc/timezone',
                ubuntu  => '/etc/timezone',
                redhat  => '/etc/sysconfig/clock',
                fedora  => '/etc/sysconfig/clock',
                centos  => '/etc/sysconfig/clock',
                suse    => '/etc/sysconfig/clock',
                freebsd => '/etc/timezone-puppet',
        }

        $commands = $::operatingsystem ? {
                debian  => '/usr/sbin/dpkg-reconfigure -f noninteractive tzdata',
                ubuntu  => '/usr/sbin/dpkg-reconfigure -f noninteractive tzdata',
                redhat  => '/usr/sbin/tzdata-update',
                fedora  => '/usr/sbin/tzdata-update',
                centos  => '/usr/sbin/tzdata-update',
                freebsd => "/bin/cp /usr/share/zoneinfo/${timezone} /etc/localtime && adjkerntz -a",
        }

        $file_content = $::operatingsystem ?{
                default => template("timezone/timezone-${operatingsystem}"),
        }

        file { 'timezone':
                ensure  => present,
                path    => $file_path,
                content => $file_content,
        }
        
        exec { 'set-timezone':
            command     => $commands,
            require     => File['timezone'],
            subscribe   => File['timezone'],
            refreshonly => true,
        }
}
