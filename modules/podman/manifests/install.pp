class podman::install {
    if $::operatingsystem == 'RedHat' {
        sm_add_repo { 'Enable rhel-7-server-extras-rpms':
            repo_name => 'rhel-7-server-extras-rpms'
        }

        Sm_add_repo['Enable rhel-7-server-extras-rpms'] -> Package['podman']
    }

    package { 'podman':
        ensure  => installed
    }
}
