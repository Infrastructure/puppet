class kvm::host {
    package {
            'libvirt': ensure => present;
            'qemu-kvm': ensure => present;
    }

    service {
        'libvirtd':
            ensure     => running,
            enable     => true,
            hasrestart => true,
            hasstatus  => true,
            require    => Package['libvirt']
    }
}

