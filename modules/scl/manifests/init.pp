class scl (
  $pool_id = undef,
) {
  if $::operatingsystem == 'CentOS' {
    package { 'centos-release-scl':
      ensure => present
    }
  }

  if $::operatingsystem == 'RedHat' {
    validate_string($pool_id)
    sm_attach_pool { 'Attach SCL pool':
      pool_id => $pool_id,
    }

    sm_add_repo { 'Enable rhel-server-rhscl-7-rpms':
        repo_name => 'rhel-server-rhscl-7-rpms',
        require   => Sm_attach_pool['Attach SCL pool'],
    }
  }
}
