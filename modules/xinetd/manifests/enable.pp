define xinetd::enable($service) {

    include xinetd::reload

    augeas { "xinetd_${service}":
        context => "/files/etc/xinetd.d/${service}/service",
        changes => 'set disable no',
        onlyif  => 'get disable != no',
        notify  => Exec['xinetd-reload']
    }
}
