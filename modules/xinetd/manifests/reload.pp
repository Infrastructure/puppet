class xinetd::reload {
    exec { 'xinetd-reload':
        command     => '/sbin/service xinetd reload',
        refreshonly => true
    }
}
