define xinetd::disable($service) {

    include xinetd::reload

    augeas { "xinetd_${service}":
        context => "/files/etc/xinetd.d/${service}/service",
        changes => 'set disable yes',
        onlyif  => 'get disable != yes',
        notify  => Exec['xinetd-reload']
    }
}
