include xinetd::reload

class xinetd::base {
    package { 'xinetd':
        ensure => installed,
    }

    $hasstatus = $::operatingsystem ? {
                debian  => false,
                default => true,
    }

    service { 'xinetd':
        enable     => true,
        ensure     => running,
        hasstatus  => $hasstatus,
        hasrestart => true,
        require    => Package['xinetd'],
    }

    file { '/etc/xinetd.d/check-mk-agent':
        ensure  => absent,
        notify  => Service[xinetd],
    }
}
