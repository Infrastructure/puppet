class kdcproxy {
    package { 'python-kdcproxy':
        ensure => present
    }

    file { '/etc/httpd/conf.d/ipa-kdc-proxy.conf':
        ensure => 'link',
        target => '/etc/ipa/kdcproxy/ipa-kdc-proxy.conf',
        notify => Service['httpd']
    }
}
