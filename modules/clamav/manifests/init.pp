class clamav {
    package {
        ['clamav', 'clamd', 'clamav-devel']: ensure => installed;
    }

    service { 'clamd@clamd':
            ensure    => running,
            enable    => true,
            hasstatus => true,
            require   => [ Package['clamd'], File['/var/run/clamav'] ]
    }

    file { '/var/run/clamav':
            ensure  => directory,
            owner   => 'clam',
            group   => 'clam',
            mode    => '0664',
            seltype => 'antivirus_var_run_t',
            require => Package['clamd'],
    }

    file { '/var/lib/clamav':
            ensure  => directory,
            owner   => 'clam',
            group   => 'clam',
            mode    => '0755',
            seltype => 'antivirus_db_t',
            require => Package['clamd'],
    }

    file { '/etc/clamd.d/clamd.conf':
            owner  => 'clam',
            group  => 'root',
            mode   => '0640',
            source => 'puppet:///modules/clamav/clamd.conf',
            notify => Service['clamd@clamd'],
    }

    file { '/etc/freshclam.conf':
            owner  => 'root',
            group  => 'root',
            mode   => '0640',
            source => 'puppet:///modules/clamav/freshclam.conf',
            notify => Service['clamd@clamd'],
    }

    file { '/etc/logrotate.d/clamav':
            ensure  => present,
            mode    => '0644',
            owner   => root,
            group   => root,
            source  => 'puppet:///modules/clamav/logrotate.d/clamav',
            require => [ Package['logrotate'] ],
    }

    file { '/etc/logrotate.d/freshclam':
            ensure  => present,
            mode    => '0644',
            owner   => root,
            group   => root,
            source  => 'puppet:///modules/clamav/logrotate.d/freshclam',
            require => [ Package['logrotate'] ],
    }
}
