class logwatch {
    package {
      'logwatch':  ensure => installed;
      'perl-Date-Manip': ensure => present;
    }

    file { '/usr/share/logwatch/default.conf/logwatch.conf':
            owner   => 'root',
            group   => 'root',
            mode    => '0644',
            content => template('logwatch/logwatch.conf.erb'),
            require => Package['logwatch']
    }

    # We'll run logwatch weekly not daily everywhere except
    # on proxy(01|02)

    if $::hostname =~ /^proxy(01|02)/ {
            file { '/etc/cron.weekly/0logwatch':
                    ensure  => 'absent',
            }

            file { '/etc/cron.daily/0logwatch':
                    owner  => 'root',
                    group  => 'root',
                    mode   => '0755',
                    source => 'puppet:///modules/logwatch/0logwatch',
            }

    } else {
            file { '/etc/cron.daily/0logwatch':
                    ensure  => 'absent',
            }

            file { '/etc/cron.weekly/0logwatch':
                    owner  => 'root',
                    group  => 'root',
                    mode   => '0755',
                    source => 'puppet:///modules/logwatch/0logwatch',
            }
    }
}
