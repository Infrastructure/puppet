class virtualization_host_kvm {
        include baseclass
        include kvm::host
        
        package { 'koan': ensure => present, }

        if $::operatingsystemrelease < '7' {
                package { 'python-virtinst':
                        ensure => present, }
        }
}
