class puppet::client {

        include puppet::variables

        $puppetmaster_load_balancer = $puppet::variables::puppetmaster_load_balancer
        $puppet_ca = $puppet::variables::puppet_ca

        if ($::operatingsystem == 'Ubuntu' and versioncmp($::operatingsystemrelease, '18') >= 0) {
            info('Puppet 5.x ships with Ubuntu 18.04, servers need update first')
        } else {
            package { 'puppet':
                ensure => present,
            }
        }

        if $::operatingsystem == 'RedHat' and $::operatingsystemrelease < 7 {
            package { 'ruby-rdoc':
                    ensure => present,
            }
        }

        service { 'puppet':
                ensure     => running,
                enable     => true,
                hasrestart => true,
                hasstatus  => true,
                require    => $::operatingsystem ? {
                  'Ubuntu' => Package['puppet-lint'],
                  default  => Package['puppet'],
                }
        }

        file { 'puppet.conf':
                ensure     => present,
                mode       => '0644',
                owner      => 'root',
                group      => 'root',
                require    => $::operatingsystem ? {
                  'Ubuntu' => Package['puppet-lint'],
                  default  => Package['puppet'],
                },
                content => template('puppet/client/puppet.conf'),
                path    => '/etc/puppet/puppet.conf',
        }

        case $::operatingsystem {
            'RedHat', 'CentOS': {
                package { 'rubygem-puppet-lint':
                    ensure => latest,
                }
            }
            'Ubuntu', 'Debian': {
                package { 'puppet-lint':
                    ensure => latest,
                }
            }
        }
}
