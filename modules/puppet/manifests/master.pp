class puppet::master (
    $is_ca = false,
) {

    validate_bool($is_ca)

    include puppet::variables

    $puppetmaster_load_balancer = $puppet::variables::puppetmaster_load_balancer
    $puppet_ca = $puppet::variables::puppet_ca

    include selinux::puppetmaster

    package { 'puppet-server':
        ensure => present,
    }

    package { 'ruby-devel':
        ensure => present,
    }

    package { 'gpgme-devel':
        ensure => present,
    }

    package { 'puppet':
        ensure => present,
    }

    package { 'pcre-tools':
        ensure => present,
    }

    package { 'rubygem-puppet-lint':
        ensure => present,
    }

    service { 'puppet':
        ensure     => running,
        enable     => true,
        hasrestart => true,
        hasstatus  => true,
        require    => Package['puppet'],
    }

    $gems = ['gpgme', 'hiera-eyaml', 'hiera-eyaml-gpg']

    package {
        $gems:
                ensure   => present,
                provider => gem,
                require  => Package['rubygems'],
    }

    file { '/etc/puppet/hieradata':
        ensure => directory,
        owner  => 'puppet',
        group  => 'sysadmin',
        mode   => '0770',
    }

    file { '/etc/puppet/hieradata/nodes':
        ensure => 'link',
        target => '/etc/puppet/hiera_nodes',
        require => File['/etc/puppet/hieradata'],
    }

    file { '/etc/eyaml':
        ensure => directory
    }

    file { '/etc/eyaml/config.yaml':
        ensure => file,
        source => 'puppet:///modules/puppet/config.yaml',
    }

    $private_dirs = ['/etc/puppet/private', '/etc/puppet/private/hiera-keys']

    file { $private_dirs:
        ensure  => directory,
        owner   => 'puppet',
        group   => 'sysadmin',
        mode    => '0660',
        seltype => 'puppet_var_lib_t',
    }

    service { 'puppetmaster':
        ensure     => running,
        enable     => true,
        hasrestart => true,
        hasstatus  => true,
        require    => Package['puppet-server']
    }

    file { 'tagmail.conf':
        ensure => present,
        path   => '/etc/puppet/tagmail.conf',
        source => 'puppet:///modules/puppet/tagmail.conf',
    }

    file { 'certificatesdir':
        ensure  => directory,
        owner   => 'puppet',
        group   => 'sysadmin',
        path    => '/srv/certificates',
        seltype => 'puppet_etc_t',
    }

    file { '/etc/puppet/auth.conf':
        ensure => 'link',
        target => '/etc/puppet-auth.conf',
    }

    file { '/etc/puppet/fileserver.conf':
        ensure => 'link',
        target => '/etc/puppet-fileserver.conf',
    }

    file { '/etc/puppet/puppet.conf':
        ensure => 'link',
        target => '/etc/puppet.conf',
    }

    file { 'auth.conf':
        ensure  => present,
        mode    => '0644',
        owner   => 'root',
        group   => 'root',
        content => template('puppet/master/puppet-auth.conf'),
        path    => '/etc/puppet-auth.conf',
    }

    file { 'fileserver.conf':
        ensure  => present,
        mode    => '0644',
        owner   => 'root',
        group   => 'root',
        content => template('puppet/master/puppet-fileserver.conf'),
        path    => '/etc/puppet-fileserver.conf',
    }

    file { 'puppet.conf':
        ensure  => present,
        mode    => '0644',
        owner   => 'root',
        group   => 'root',
        content => template('puppet/master/puppet.conf'),
        path    => '/etc/puppet.conf',
        seltype => 'puppet_etc_t',
    }
}
