[main]
    # Where Puppet stores dynamic and growing data.
    # The default value is '/var/puppet'.
    vardir = /var/lib/puppet

    # The Puppet log directory.
    # The default value is '$vardir/log'.
    logdir = /var/log/puppet

    # Where Puppet PID files are kept.
    # The default value is '$vardir/run'.
    rundir = /var/run/puppet

    # Where SSL certificates are kept.
    # The default value is '$confdir/ssl'.
    ssldir = $vardir/ssl

    tagmap = /etc/puppet/tagmail.conf

    reportfrom = noreply@gnome.org

    reports = tagmail

    # The puppet master server to request configurations from.
    server = <%= @fqdn %>

    # puppetmaster01 acts as the CA server (no particular reason
    # to have failover for this particular service)
    ca_server = <%= @puppet_ca %> 

[agent]
    # The file in which puppetd stores a list of the classes
    # associated with the retrieved configuratiion.  Can be loaded in
    # the separate ``puppet`` executable using the ``--loadclasses``
    # option.
    # The default value is '$confdir/classes.txt'.
    classfile = $vardir/classes.txt

    # Where puppetd caches the local configuration.  An
    # extension indicating the cache format is added automatically.
    # The default value is '$confdir/localconfig'.
    localconfig = $vardir/localconfig

[master]
    <%- if @is_ca -%>
    autosign = /etc/puppet/autosign.conf
    dns_alt_names = <%= @puppetmaster_load_balancer %>
    <%- else -%>
    ca = false
    <%- end -%>
