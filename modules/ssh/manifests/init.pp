class ssh (
    $ssh_keys_lookaside = false,
) {

    package { 'openssh-server': ensure => present }

    service { 'sshd':
        ensure     => running,
        enable     => true,
        hasrestart => true,
        hasstatus  => true,
        require    => Package['openssh-server']
    }

    if ($ssh_keys_lookaside) {

      file { '/etc/sshd':
            ensure => directory,
            mode   => '0711',
            owner  => 'root',
            group  => 'root'
      }

      file { '/etc/sshd/users':
            ensure => directory,
            mode   => '0711',
            owner  => 'root',
            group  => 'root'
      }
    }

    file { 'sshd_config':
        ensure  => present,
        path    => '/etc/ssh/sshd_config',
        mode    => '0600',
        owner   => root,
        group   => root,
        content => template('ssh/sshd_config'),
        require => Package['openssh-server'],
        notify  => Service['sshd']
    }
}
