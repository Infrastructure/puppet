class fishpoll::server {

    package { 'fishpolld': ensure => latest }

    if ($::operatingsystem == 'RedHat' or $::operatingsystem == 'CentOS') and $::operatingsystemrelease =~ /^7\./ {
            $servicename = 'fishpolld'
            $packagename = 'fishpolld'
    } elsif ($::operatingsystem == 'RedHat' or $::operatingsystem == 'CentOS') and $::operatingsystemrelease < '7' {
            $servicename = 'fishpoll'
            $packagename = 'fishpolld'
    } elsif ($::operatingsystem == 'Ubuntu' or $::operatingsystem == 'Debian') {
            $servicename = 'fishpolld'
            $packagename = 'fishpolld'
    }

    file { '/etc/fishpoll.d':
            ensure  => directory,
            recurse => true,
            purge   => true,
            force   => true,
            owner   => 'root',
            group   => 'root',
            mode    => '0755',
            source  => 'puppet:///modules/fishpoll/fishpoll.d-empty',
            notify  => Service["$servicename"],
    }

    service { "$servicename":
            ensure     => running,
            enable     => true,
            hasrestart => true,
            hasstatus  => true,
            require    => Package["$packagename"]
    }

    case $::operatingsystem {

        'redhat','centos': {
            file { 'fishpolld.sysconfig':
                ensure  => present,
                path    => '/etc/sysconfig/fishpolld',
                mode    => '0644',
                owner   => 'root',
                group   => 'root',
                content => template('fishpoll/fishpolld.sysconfig'),
                require => Package["$packagename"],
                notify  => Service["$servicename"],
            }
        }

        'ubuntu', 'debian' : {
            file { 'fishpolld.default':
                ensure  => present,
                path    => '/etc/default/fishpolld',
                mode    => '0644',
                owner   => 'root',
                group   => 'root',
                content => template('fishpoll/fishpolld.default'),
                require => Package["$packagename"],
                notify  => Service["$servicename"],
            }
        }
    }
}
