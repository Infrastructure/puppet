define fishpoll::hook($owner='root', $group='root') {

    include fishpoll::server

    $packagename = $fishpoll::server::packagename
    $servicename = $fishpoll::server::servicename

    file { "${name}":
        path    => "/etc/fishpoll.d/${name}",
        mode    => '0755',
        owner   => $owner,
        group   => $group,
        source  => "puppet:///modules/fishpoll/hooks/${name}",
        require => Package["${packagename}"],
    }

    file { "${name}.conf":
        path    => "/etc/fishpoll.d/${name}.conf",
        mode    => '0644',
        owner   => $owner,
        group   => $group,
        source  => "puppet:///modules/fishpoll/hooks/default.conf",
        require => Package["${packagename}"],
        notify  => Service["${servicename}"],
    }
}
