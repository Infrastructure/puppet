class fishpoll::client {
        package {
                'fishpoll': ensure => absent;
                'fishpoke': ensure => present;
        }
}
