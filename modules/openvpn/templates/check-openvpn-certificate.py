#!/usr/bin/python

import time
import sys
import os
import subprocess
from datetime import datetime, timedelta

CERTNAME = sys.argv[1]
CERTFULLPATH = os.path.join('<%= @openvpn_certs_path %>', CERTNAME)
NOW = datetime.now()

try:
    os.path.getsize(CERTFULLPATH)
except OSError:
    print 'CRITICAL: ' + CERTNAME + ' was not found, please verify its path'
    sys.exit(2)

proc = subprocess.Popen(['openssl', 'x509', '-in', CERTFULLPATH, '-text', '-noout'], stdout=subprocess.PIPE)
output = proc.stdout.readlines()

for line in output:
    if 'Not After' in line:
        date = line.split()

polished_date = date[4] + ' ' + date[3] + ' ' + date [6]

expiration_date = datetime.strptime(polished_date, "%d %b %Y")
time_delta = expiration_date - NOW

if time_delta.days <= 10 and time_delta.days > 3:
    print 'WARNING: %s is going to expire within 10 days from now, expiration date is %s' % (CERTNAME, polished_date)
    sys.exit(1)
elif time_delta.days <= 3 and time_delta.days > 0:
    print 'CRITICAL: %s is going to expire within 3 days from now, expiration date is %s' % (CERTNAME, polished_date)
    sys.exit(2)
elif time_delta.days <= 0:
    print 'CRITICAL: %s has expired on %s, please renew the certificate' % (CERTNAME, polished_date)
    sys.exit(2)
else:
    print 'OK: the %s certificate is going to expire on %s' % (CERTNAME, polished_date)
    sys.exit(0)
