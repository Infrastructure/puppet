class openvpn::server {

        $openvpn_certs_path = '/etc/openvpn/pki/issued'

        package {
                'openvpn': ensure => installed;
                'easy-rsa': ensure => installed;
        }

        service {'openvpn@server':
                enable     => true,
                ensure     => running,
                hasstatus  => true,
                hasrestart => true,
                require    => Package[openvpn],
            }

        file { 'server.conf':
                path   => '/etc/openvpn/server.conf',
                mode   => '0644',
                owner  => root,
                group  => root,
                ensure => present,
                source => 'puppet:///modules/openvpn/server.conf'
            }

        file { 'vars':
                path   => '/etc/openvpn/easy-rsa/vars',
                mode   => '0644',
                owner  => root,
                group  => root,
                ensure => present,
                source => 'puppet:///modules/openvpn/vars'
        }

        file { '/etc/openvpn/ccd':
                ensure  => 'directory',
                recurse => true,
                owner   => 'nobody',
                group   => 'nobody',
                mode    => '0644',
                source  => 'puppet:///modules/openvpn/static-ips',
        }

        file { '/etc/openvpn/easy-rsa':
                ensure => symlink,
                target => '/usr/share/easy-rsa/3.0'
        }

        file { '/etc/openvpn/pki':
                ensure => symlink,
                target => '/etc/openvpn/easy-rsa/pki'
        }

        file { '/etc/openvpn/crl.pem':
                ensure => symlink,
                target => '/etc/openvpn/pki/crl.pem',
                notify => Service['openvpn@server']
        }

        file { '/usr/lib64/nagios/plugins/check-openvpn-certificate.py':
                owner    => 'root',
                group    => 'root',
                mode     => '0755',
                content  => template('openvpn/check-openvpn-certificate.py'),
        }
}
