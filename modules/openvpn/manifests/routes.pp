class openvpn::routes {

    if $::ipaddress_br1 =~ /^172.31.2/ {
      $InternalNic = 'br1'
    } elsif $::ipaddress_br2 =~ /^172.31.2/ {
      $InternalNic = 'br2'
    } else {
      $InternalNic = 'eth1'
    }

    file { "route-${InternalNic}":
            path   => "/etc/sysconfig/network-scripts/route-${InternalNic}",
            mode   => '0644',
            owner  => root,
            group  => root,
            ensure => present,
            source => 'puppet:///modules/openvpn/route-vpn',
    }
}
