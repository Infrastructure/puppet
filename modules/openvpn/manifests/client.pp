class openvpn::client {

        $openvpn_certs_path = '/etc/openvpn/keys'

        package { 'openvpn':
                ensure => installed;
        }

        if $::operatingsystem == 'RedHat' and $::operatingsystemrelease =~ /^7\./ {
                $service_name = 'openvpn@client'
        } elsif $::operatingsystem == 'CentOS' and $::operatingsystemrelease =~ /^7\./ {
                $service_name = 'openvpn@client'
        } else {
                $service_name = 'openvpn'
        }

        case $::operatingsystem {
                redhat,centos: {
                        $library_arch = 'lib64'
                }
                ubuntu,debian: {
                        $library_arch = 'lib'
                }
        }

        if $::operatingsystem == 'RedHat' and $::operatingsystemrelease =~ /^7\./ {

                service {'openvpn@client':
                        enable      => true,
                        ensure      => running,
                        hasstatus   => true,
                        hasrestart  => true,
                        require     => Package[openvpn],
                }

        } elsif $::operatingsystem == 'CentOS' and $::operatingsystemrelease =~ /^7\./ {

                service {'openvpn@client':
                        enable      => true,
                        ensure      => running,
                        hasstatus   => true,
                        hasrestart  => true,
                        require     => Package[openvpn],
                }

        } else {

                service {'openvpn':
                        enable      => true,
                        ensure      => running,
                        hasstatus   => true,
                        hasrestart  => true,
                        require     => Package[openvpn],
                }
        }

        file { 'client.conf':
                        path    => '/etc/openvpn/client.conf',
                        mode    => '0644',
                        owner   => root,
                        group   => root,
                        ensure  => present,
                        content => template('openvpn/client.conf.erb'),
                        require     => Package[openvpn],
        }

        file { '/etc/openvpn/keys':
                        ensure  => directory,
                        mode    => '0755',
                        owner   => root,
                        group   => root,
                        require => Package['openvpn']
        }

        file { "non-phx2-machines-${hostname}.crt":
                        path   => "/etc/openvpn/keys/non-phx2-machines-${hostname}.crt",
                        mode   => '0644',
                        owner  => root,
                        group  => root,
                        ensure => present,
                        source => "puppet:///certificates/openvpn/non-phx2-machines-${hostname}.crt",
                        notify => Service["${service_name}"]
        }

        file { "non-phx2-machines-${hostname}.key":
                        path   => "/etc/openvpn/keys/non-phx2-machines-${hostname}.key",
                        mode   => '0600',
                        owner  => root,
                        group  => root,
                        ensure => present,
                        source => "puppet:///certificates/openvpn/non-phx2-machines-${hostname}.key",
                        notify => Service["${service_name}"]
        }

        file { 'ca.crt':
                        path   => '/etc/openvpn/keys/ca.crt',
                        mode   => '0644',
                        owner  => root,
                        group  => root,
                        ensure => present,
                        source => 'puppet:///certificates/openvpn/cacerts/ca.crt',
                        notify => Service["${service_name}"]
        }

        file { 'ta.key':
                        path   => '/etc/openvpn/keys/ta.key',
                        mode   => '0600',
                        owner  => root,
                        group  => root,
                        ensure => present,
                        source => 'puppet:///certificates/openvpn/cacerts/ta.key',
                        notify => Service["${service_name}"]
        }

        file { "/usr/${library_arch}/nagios/plugins/check_openvpn":
                        owner  => 'root',
                        group  => 'root',
                        mode   => '0755',
                        source => 'puppet:///modules/openvpn/check_openvpn',
        }

        file { "/usr/${library_arch}/nagios/plugins/check-openvpn-certificate.py":
                        owner  => 'root',
                        group  => 'root',
                        mode   => '0755',
                        content    => template('openvpn/check-openvpn-certificate.py'),
        }
}
