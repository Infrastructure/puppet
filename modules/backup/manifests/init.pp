# Class to define backup of SSH keys
class backup {

    package { 'rdiff-backup':
        ensure => 'installed'
    }

    package { 'rsync':
        ensure => installed
    }

    file { '/root/.ssh/':
        ensure => directory,
        mode   => '0700',
        owner  => 'root',
        group  => 'root'
    }

    file { '/root/.ssh/authorized_keys':
        ensure  => present,
        mode    => '0400',
        owner   => 'root',
        group   => 'root',
        source  => 'puppet:///certificates/backup_authorized_keys',
        require => File['/root/.ssh/']
    }

    file { '/etc/rsyncd/':
        ensure => directory,
        mode   => '0755',
        owner  => 'root',
        group  => 'root'
    }

    file { 'backup.exclude':
        ensure  => present,
        path    =>'/etc/rsyncd/backup.exclude',
        mode    => '0444',
        owner   => 'root',
        group   => 'root',
        content => template('backup/backup.exclude'),
        require => File['/etc/rsyncd/']
    }
}


