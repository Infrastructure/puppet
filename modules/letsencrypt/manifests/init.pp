class letsencrypt {

        file { '/srv/letsencrypt':
                ensure => directory,
                owner  => 'letsencrypt',
                group  => 'letsencrypt',
        }

        file { '/srv/letsencrypt/certificates':
                ensure  => directory,
                recurse => true,
                owner   => 'letsencrypt',
                group   => 'puppet',
                seltype => 'puppet_etc_t',
        }

        file { '/srv/letsencrypt/certificates/keys':
                ensure  => directory,
                recurse => true,
                mode    => '0660',
                owner   => 'puppet',
                group   => 'letsencrypt',
                seltype => 'puppet_etc_t',
        }

        file { '/srv/letsencrypt/configurations':
                ensure => directory,
                owner  => 'letsencrypt',
                group  => 'letsencrypt',
        }

        file { '/srv/letsencrypt/getssl':
                owner  => 'letsencrypt',
                group  => 'letsencrypt',
                mode   => '0700',
                source => 'puppet:///modules/letsencrypt/getssl',
        }

        include acl
        acl::group::acl_group { 'srv_letsencrypt_certificates_r':
                id   => 'puppet',
                mode => 'r--',
                path => '/srv/letsencrypt/certificates/keys',
        }

        acl::gdefault::acl_gdefault { 'srv_letsencrypt_certificates_default_r':
                id   => 'puppet',
                mode => 'r--',
                path => '/srv/letsencrypt/certificates/keys',
        }

        cron { "Automatic renewal of Let's Encrypt SSL certificates":
                command     => '/srv/letsencrypt/getssl -w /srv/letsencrypt/configurations -a',
                special     => 'monthly',
                user        => letsencrypt,
                environment => 'MAILTO=root',
        }
}
