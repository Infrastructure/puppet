#!/bin/bash

NEW_CERT="$1"
NEW_PRIVATE_KEY="$2"
PROJECT="$3"
ROUTE_NAME="$4"
SERVICE="$5"
ROUTE_TYPE="$6"
HOSTNAME="$7"

oc project $PROJECT

oc delete route $ROUTE_NAME

oc create route $ROUTE_TYPE $ROUTE_NAME --insecure-policy="Redirect" \
    --service="${SERVICE}" --cert="/etc/pki/tls/certs/$NEW_CERT" \
    --key="/etc/pki/tls/private/${NEW_PRIVATE_KEY}" \
    --ca-cert="/etc/pki/tls/letsencrypt.intermediate.ca.pem" \
    --hostname="${HOSTNAME}"
