#!/bin/bash
## This script is intended to replace the default SSL certificate assigned to the OpenShift router pods
## This process can be peformed without service disruption, rather than re-deploying the routers (which has a service impact)
## The SSL certificate contents are mounted as a secret inside of the router pods, which this script will replace and re-deploy
## You must have cluster-admin privileges, and this assumes the router deployment config has the default name

NEW_CERT="$1"
NEW_PRIVATE_KEY="$2"
NEW_PEM='/root/router_certs.pem'

# Select the project
oc project default

# Create a pem file with the new files
cat /etc/pki/tls/certs/$NEW_CERT /etc/pki/tls/private/$NEW_PRIVATE_KEY > $NEW_PEM 

# Remove existing secret
oc delete secret router-certs

# Create new secret
oc secrets new router-certs tls.crt=$NEW_PEM tls.key=/etc/pki/tls/private/$NEW_PRIVATE_KEY --type='kubernetes.io/tls' --confirm

# Redeploy router pods
oc rollout latest dc/router
