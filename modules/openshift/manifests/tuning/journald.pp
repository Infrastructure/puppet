class openshift::tuning::journald {

        exec { 'systemd-journald-reload':
            command     => '/usr/bin/systemctl restart systemd-journald',
            refreshonly => true,
        }

        if ($::fqdn =~ /^oscp\-(master|router)/) {
            $changes = ['set /files/etc/systemd/journald.conf/Journal/ForwardToSyslog True',
                        'set /files/etc/systemd/journald.conf/Journal/SystemMaxUse 1G'
                        ]
        } else {
            $changes = [ 'set /files/etc/systemd/journald.conf/Journal/SystemMaxUse 4G' ]
        }

        augeas { 'openshift_tuning':
                incl    => '/etc/systemd/journald.conf',
                lens    => 'Puppet.lns',
                changes => $changes,
                notify  => Exec['systemd-journald-reload'],
        }
}
