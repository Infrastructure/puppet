define openshift::routers::renew_edge_certs($name, $ssl_cert_name, $requires_chain=true) {
        realize(Package['openssl'])

        file { '/usr/local/bin/renew_edge_certs.sh':
            ensure => present,
            mode   => '0755',
            owner  => 'root',
            group  => 'root',
            source => 'puppet:///modules/openshift/renew_edge_certs.sh',
        }

        if $requires_chain {

            file { "/etc/pki/tls/certs/${name}.crt":
                ensure  => present,
                mode    => '0644',
                owner   => 'root',
                group   => 'root',
                source  => "puppet:///letsencrypt/requires_chain/${ssl_cert_name}.full.crt",
                require => Package['openssl'],
            }

        } else {

            file { "/etc/pki/tls/certs/${name}.crt":
                ensure  => present,
                mode    => '0644',
                owner   => 'root',
                group   => 'root',
                source  => "puppet:///letsencrypt/crts/${ssl_cert_name}.crt",
                require => Package['openssl'],
            }
        }

        file { "/etc/pki/tls/private/${name}.key":
            ensure  => present,
            mode    => '0600',
            owner   => 'root',
            group   => 'root',
            source  => "puppet:///letsencrypt/keys/${ssl_cert_name}.key",
            require => Package['openssl'],
        }

        exec {"renew_edge_certs_for_${name}":
                command     => "/usr/local/bin/renew_edge_certs.sh ${name}.crt ${name}.key",
                subscribe   => File["/etc/pki/tls/certs/${name}.crt"],
                refreshonly => true,
                require     => File['/usr/local/bin/renew_edge_certs.sh'],
                user        => 'root',
                environment => 'KUBECONFIG=/root/.kube/config',
        }
}
