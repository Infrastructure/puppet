define openshift::routers::renew_route_certs($ssl_cert_name, $project_name, $oc_hostname, $route_name, $service_name, $route_type, $requires_chain=false) {
        realize(Package['openssl'])

        if $requires_chain {

            file { "${ssl_cert_name}-${name}":
                path    => "/etc/pki/tls/certs/${ssl_cert_name}-${name}.crt",
                ensure  => present,
                mode    => '0644',
                owner   => 'root',
                group   => 'root',
                source  => "puppet:///letsencrypt/requires_chain/${ssl_cert_name}.full.crt",
                require => Package['openssl'],
            }

        } else {
            file { "${ssl_cert_name}-${name}-crt":
                path    => "/etc/pki/tls/certs/${ssl_cert_name}-${name}.crt",
                ensure  => present,
                mode    => '0644',
                owner   => 'root',
                group   => 'root',
                source  => "puppet:///letsencrypt/crts/${ssl_cert_name}.crt",
                require => Package['openssl'],
            }
        }

        file { "${ssl_cert_name}-${name}-key":
            path    => "/etc/pki/tls/private/${ssl_cert_name}-${name}.key",
            ensure  => present,
            mode    => '0600',
            owner   => 'root',
            group   => 'root',
            source  => "puppet:///letsencrypt/keys/${ssl_cert_name}.key",
            require => Package['openssl'],
        }

        exec {"renew_route_certs_for_${name}":
                command     => "/usr/local/bin/renew_route_certs.sh ${ssl_cert_name}-${name}.crt ${ssl_cert_name}-${name}.key ${project_name} ${route_name} ${service_name} ${route_type} ${oc_hostname}",
                subscribe   => File["/etc/pki/tls/certs/${ssl_cert_name}-${name}.crt"],
                refreshonly => true,
                require     => File['/usr/local/bin/renew_route_certs.sh'],
                user        => 'root',
                environment => 'KUBECONFIG=/root/.kube/config',
        }
}
