class openshift::routes {

    file { '/usr/local/bin/renew_route_certs.sh':
        ensure => present,
        mode   => '0755',
        owner  => 'root',
        group  => 'root',
        source => 'puppet:///modules/openshift/renew_route_certs.sh',
    }
}
