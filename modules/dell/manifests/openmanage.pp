class dell::openmanage {

    include packages::dell

    package { [
        'srvadmin-base',
        'srvadmin-storageservices',
        'srvadmin-omcommon',
        'OpenIPMI',
        'OpenIPMI-libs',
        'srvadmin-idrac',
        'srvadmin-idracadm7',
        'srvadmin-server-cli',
        'srvadmin-server-snmp',
        'nagios-plugins-openmanage',
        'net-snmp',
        'net-snmp-utils',
        ]:
        ensure  => installed,
        require => File['/etc/yum.repos.d/dell-omsa-repository.repo'],
    }

    service { 'ipmi':
            ensure     => running,
            enable     => true,
            hasrestart => true,
            hasstatus  => true,
            require    => Package['OpenIPMI']
    }

    service { 'snmpd':
            ensure     => running,
            enable     => true,
            hasrestart => true,
            hasstatus  => true,
            require    => [Package['net-snmp'], Package['net-snmp-utils']]
    }

    file { '/etc/snmp/snmpd.conf':
            owner  => 'root',
            group  => 'root',
            mode   => '0640',
            source => 'puppet:///modules/dell/snmpd.conf',
            notify => Service[snmpd],
    }

    exec { 'start_srvadmin_services_if_stopped_not_running':
            command => '/opt/dell/srvadmin/sbin/srvadmin-services.sh start',
            unless  => '/opt/dell/srvadmin/sbin/srvadmin-services.sh status',
    }
}
