class mysql {

  if ($::operatingsystem == 'RedHat' or $::operatingsystem == 'CentOS') and $::operatingsystemmajrelease < 7 {
          $service_name = 'mysqld'
          $package_name = 'mysql-server'

          package { 'mysql-server': ensure => installed }

  } elsif ($::operatingsystem == 'RedHat' or $::operatingsystem == 'CentOS') and $::operatingsystemmajrelease >= 7 {
          $service_name = 'mariadb'
          $package_name = 'mariadb-server'

          package { 'mariadb-server': ensure => installed }

          file { 'mysqld-slow.log':
                  ensure  => present,
                  path    => '/var/log/mysqld-slow.log',
                  mode    => '0644',
                  owner   => 'mysql',
                  group   => 'mysql',
                  seltype => 'mysqld_log_t',
          }
  }

  package { 'MySQL-python': ensure => present }

  service { "${service_name}":
          ensure     => running,
          enable     => true,
          hasrestart => true,
          hasstatus  => true,
          require    => Package["${package_name}"]
  }

  file { 'my.cnf':
            ensure  => present,
            path    => '/etc/my.cnf',
            mode    => '0644',
            owner   => root,
            group   => root,
            content => template('mysql/my.cnf.default'),
            require => Package["${package_name}"],
            notify  => Service["${service_name}"]
        }
}
