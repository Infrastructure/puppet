class mysql::master inherits mysql {

  file { 'copy-db.exclude':
          ensure  => present,
          path    => '/etc/copy-db.exclude',
          mode    => '0644',
          owner   => root,
          group   => root,
          content => template('mysql/copy-db.exclude')
  }

  cron { 'copy-db.py':
          command => '/home/admin/bin/copy-db.py',
          user    => root,
          hour    => 0,
          minute  => 47,
          require => [ File['/etc/copy-db.exclude'] ]
  }
}
