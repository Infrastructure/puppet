class gnome_users (
    $create_auth_via_glusterfs = true,
) {

    $ldap_ro_password = hiera(freeipa::ldap_ro_password)

    class { 'ssh':
        ssh_keys_lookaside => true,
    }

    package { 'python-ldap': ensure => present }

    if (!$create_auth_via_glusterfs) {
        File {
            mode  => '0755',
            owner => 'root',
            group => 'root',
        }

        file { '/home/admin': ensure => directory }
        file { '/home/admin/secret': ensure => directory, require => File['/home/admin'] }
        file { '/home/admin/bin': ensure => directory, require => File['/home/admin'] }

        file { 'freeipa_ro':
            path    => '/home/admin/secret/freeipa_ro',
            mode    => '0400',
            owner   => root,
            group   => root,
            ensure  => present,
            content => template('gnome_users/freeipa_ro'),
            require => File['/home/admin/secret'],
        }

        vcsrepo { '/home/admin/bin':
            ensure   => latest,
            provider => git,
            source   => 'https://gitlab.gnome.org/Infrastructure/sysadmin-bin.git',
            revision => 'master',
        }
    }

    file { 'create-auth':
            path    => '/etc/cron.hourly/create-auth',
            mode    => '0755',
            owner   => root,
            group   => root,
            ensure  => present,
            content => template('gnome_users/create-auth'),
            require => Package['python-ldap']
    }
}
