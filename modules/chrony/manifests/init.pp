class chrony (
    $disable_ntp = true
) {
    validate_bool($disable_ntp)

    package {
            'chrony': ensure => present
    }
        
    service { 'chronyd':
            ensure     => running,
            enable     => true,
            hasrestart => true,
            hasstatus  => true,
            require    => Package['chrony']
    }

    file { '/etc/chrony.conf':
            ensure  => present,
            mode    => '0644',
            owner   => root,
            group   => root,
            content => template('chrony/chrony.conf.erb'),
            require => Package['chrony'],
            notify  => Service['chronyd']
    }

    if ($disable_ntp) {
        service { 'ntpd':
                ensure => stopped,
                enable => false,
        }
    }
}
