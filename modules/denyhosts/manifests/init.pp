class denyhosts {
    package { 'denyhosts':
        ensure => installed,
    }

    service { 'denyhosts':
        ensure    => running,
        enable    => true,
        hasstatus => true,
        require   => Package['denyhosts'],
    }

    case $::operatingsystem {
        'CentOS', 'RedHat', 'Fedora': {
            file { '/etc/denyhosts.conf':
                source  => 'puppet:///modules/denyhosts/denyhosts.conf.secure',
                owner   => 'root',
                group   => 'root',
                mode    => '0644',
                notify  => Service['denyhosts'],
                require => Package['denyhosts'],
            }
        }
        'Debian', 'Ubuntu': {
            file { '/etc/denyhosts.conf':
                source  => 'puppet:///modules/denyhosts/denyhosts.conf.auth',
                owner   => 'root',
                group   => 'root',
                mode    => '0644',
                notify  => Service['denyhosts'],
                require => Package['denyhosts'],
            }
        }
    }

    file { '/var/lib/denyhosts/allowed-hosts':
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => 'puppet:///modules/denyhosts/allowed-hosts',
        notify  => Service['denyhosts'],
        require => Package['denyhosts'],
    }
}

