install
reboot
rootpw --iscrypted <%= @kickstart_default_password %>
url --url=$tree
lang en_US.UTF-8
keyboard us
network --onboot yes --bootproto=static --ip=$private_ip --netmask=255.255.255.0 --gateway=$gateway --device=$device_name --noipv6 --hostname=$hostname
firewall --service=ssh
authconfig --enableshadow --passalgo=sha512
selinux --enforcing
timezone --utc America/Phoenix
bootloader --location=mbr --boot-drive=sda
clearpart --all --drives=sda

part /boot/efi --fstype=efi --grow --maxsize=200 --size=50
part /boot --fstype=xfs --size=500
part pv.008003 --grow --size=1

volgroup $vg_name --pesize=4096 pv.008003
logvol /home --fstype=xfs --name=lv_home --vgname=$vg_name --size=3000
logvol / --fstype=xfs --name=lv_root --vgname=$vg_name --size=51200
logvol swap --name=lv_swap --vgname=$vg_name --size=32248

%post --logfile /root/ks-post.log
echo 'nameserver 172.31.2.7' >> /etc/resolv.conf
echo 'nameserver 172.31.2.29' >> /etc/resolv.conf

$SNIPPET('enable_repositories')
$SNIPPET('configure_puppet')
%end

%packages
bash-completion
crontabs
iptables-services
mailx
ntp
ntpdate
openssh-clients
openssh-server
patch
postfix
rsync
screen
telnet
tmpwatch
traceroute
-sendmail
-sendmail-cf
strace
subscription-manager
tmux
vim-enhanced
yum
yum-rhn-plugin
yum-utils
deltarpm
redhat-lsb
wget
git
%end
