install
reboot
rootpw --iscrypted $6$uXe0ieHeiv$/VcMCNTqZSpQdDaAvrYQqAj51HlqUsEk0.5dHWDFDPqvABWvhUnGNlpK78/beKjQfgfBoJ.G9I2Qu9Jq2imns0
url --url=$tree
lang en_US.UTF-8
keyboard us
network --onboot yes --bootproto=static --ip=$private_ip --netmask=255.255.255.0 --gateway=$gateway --device=$device_name --noipv6 --hostname=$hostname
firewall --service=ssh
authconfig --enableshadow --passalgo=sha512
selinux --enforcing
timezone --utc UTC
bootloader 
clearpart --all --initlabel

part /boot/efi --fstype efi --grow --maxsize=250 --ondrive=sda
part /boot/efi-back --fstype vfat --maxsize=250 --ondrive=sdb

part raid.0001 --size=1000 --ondisk=sda
part raid.0002 --size=1000 --ondisk=sdb

part raid.0011 --size=200 --grow --ondisk sda
part raid.0012 --size=200 --grow --ondisk sdb

raid /boot --fstype=xfs --level=1 --device=md0 raid.0001 raid.0002
raid pv.008003          --level=1 --device=md1 raid.0011 raid.0012

volgroup VolGroup00  --pesize=4096 pv.008003
logvol /home --fstype=xfs --name=lv_home --vgname=VolGroup00 --size=51200
logvol / --fstype=xfs --name=lv_root --vgname=VolGroup00 --size=256000 

%post --logfile /root/ks-post.log
echo 'nameserver 172.31.2.7' >> /etc/resolv.conf
%end

%packages
bash-completion
crontabs
iptables-services
mailx
openssh-clients
openssh-server
patch
postfix
rsync
telnet
traceroute
-sendmail
-sendmail-cf
strace
tmux
vim-enhanced
wget
git
%end
