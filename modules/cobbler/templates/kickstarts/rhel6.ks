# Documentation
# http://fedoraproject.org/wiki/Anaconda/Kickstart

auth  --useshadow  --enablemd5
bootloader --location=mbr
clearpart --all --initlabel
text
firstboot --disable
keyboard us
lang en_US.UTF-8
timezone --utc Etc/GMT

url --url=$tree
# If any cobbler repo definitions were referenced in the kickstart profile, include them here.
$yum_repo_stanza

# Network information
$SNIPPET('network_config')

# Reboot after installation
reboot

#rootpw --iscrypted $default_password_crypted
rootpw --iscrypted <%= @kickstart_default_password %>
selinux --disabled
skipx

install
zerombr

# Services configuration
#services --disabled gpm,messagebus,,mdmonitor,xinetd
#services --enabled network,postfix,sshd,ntpd,sysstat,ntpdate,iptables,mdmonitor

# Partitioning
$SNIPPET('main_partition_select')

%pre
$SNIPPET('log_ks_pre')
$kickstart_start
$SNIPPET('pre_install_network_config')

# Enable installation monitoring
$SNIPPET('pre_anamon')
$SNIPPET('pre_partition_select')
%end

%packages
$SNIPPET('func_install_if_enabled')
@core
@base
sysstat
emacs-nox
vim-enhanced
portmap
nfs-utils

%post --log=/root/kickstart-post.log
set -vx

# Start yum configuration
$yum_config_stanza
# End yum configuration

$kickstart_done
$SNIPPET('post_install_kernel_options')
$SNIPPET('post_install_network_config')
$SNIPPET('func_register_if_enabled')
$SNIPPET('download_config_files')
$SNIPPET('koan_environment')
$SNIPPET('cobbler_register')

# Enable post-install boot notification
$SNIPPET('post_anamon')

# Keep the system from getting in an endless kickstart loop
$kickstart_done
%end
