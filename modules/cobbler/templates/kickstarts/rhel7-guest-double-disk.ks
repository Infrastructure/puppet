rootpw --iscrypted <%= @kickstart_default_password %>

text
url --url=$tree

bootloader
clearpart --all --initlabel
part /boot --fstype ext4 --size 400 --ondisk=vda
part / --fstype xfs --size 1 --grow --ondisk=vda
part swap --recommended --ondisk=vda
part $vdb_mount_point --fstype xfs --size 1 --grow --ondisk=vdb

services --enabled=ntpd,postfix,rsyslog,sshd --disabled=firewalld

network --bootproto=static --ip=$private_ip --netmask=255.255.255.0 --gateway=$gateway --device=eth1 --hostname=$hostname

firewall --disabled
lang en_US
timezone --utc Etc/GMT
keyboard us
lang en_US.UTF-8

reboot
install

selinux --enforcing

%packages
bash-completion
crontabs
iptables-services
mailx
ntp
ntpdate
openssh-clients
openssh-server
patch
postfix
rsync
screen
telnet
tmpwatch
traceroute
-sendmail
-sendmail-cf
strace
subscription-manager
tmux
vim-enhanced
yum
yum-rhn-plugin
yum-utils
deltarpm
redhat-lsb
wget
git
%end

%post --log=/root/ks-post.log
echo 'nameserver 172.31.2.7' >> /etc/resolv.conf
echo 'nameserver 172.31.2.29' >> /etc/resolv.conf

$SNIPPET('enable_repositories')
$SNIPPET('configure_puppet')
%end
