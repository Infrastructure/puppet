class cobbler {

    include cobbler::variables

    $rhn_password = $cobbler::variables::rhn_password
    $storage_path = $cobbler::variables::storage_path
    $kickstart_default_password = $cobbler::variables::kickstart_default_password
    $rhgs_pool_id = $cobbler::variables::rhgs_pool_id

    package { 'cobbler':
        ensure => present
    }

    package { 'dhcp':
        ensure => present
    }

    package { 'tftp':
        ensure => present
    }

    service { 'cobblerd':
        ensure  => running,
        enable  => true,
        require => Package['cobbler']
    }

    File {
        ensure  => 'present',
        mode    => '0644',
        require => Package['cobbler'],
    }

    file { '/etc/cobbler/settings':
        content => template('cobbler/settings'),
        notify  => Service['cobblerd'],
    }

    file {
      '/var/lib/cobbler/kickstarts/rhel6.ks':
        content => template('cobbler/kickstarts/rhel6.ks');

      '/var/lib/cobbler/kickstarts/rhel7-guest-single-disk.ks':
        content => template('cobbler/kickstarts/rhel7-guest-single-disk.ks');

      '/var/lib/cobbler/kickstarts/rhel7-guest-double-disk.ks':
        content => template('cobbler/kickstarts/rhel7-guest-double-disk.ks');

      '/var/lib/cobbler/kickstarts/baremetal-rhel7.ks':
        content => template('cobbler/kickstarts/baremetal-rhel7.ks');

      '/var/lib/cobbler/snippets/enable_repositories':
        content => template('cobbler/snippets/enable_repositories.erb');

      '/var/lib/cobbler/snippets/configure_puppet':
        source => 'puppet:///modules/cobbler/snippets/configure_puppet';

      '/etc/cobbler/dhcp.template':
        source => 'puppet:///modules/cobbler/dhcp.template';

      '/var/www/cobbler/repo_mirror':
        ensure => link,
        force  => true,
        target => "${storage_path}/repo_mirror";

      '/var/www/cobbler/images':
        ensure => link,
        force  => true,
        target => "${storage_path}/images";

      '/var/www/cobbler/ks_mirror':
        ensure => link,
        force  => true,
        target => "${storage_path}/ks_mirror";

      '/var/www/cobbler/links':
        ensure => link,
        force  => true,
        target => "${storage_path}/links";
    }

    selboolean { 'httpd_can_network_connect_cobbler':
            name       => 'httpd_can_network_connect_cobbler',
            persistent => true,
            value      => on
    }
}
