class cobbler::variables {

    $rhn_password = hiera('cobbler::rhn_password')
    $rhgs_pool_id = hiera('cobbler::rhgs_pool_id')
    $storage_path = '/srv/cobbler_data'
    $kickstart_default_password = hiera('cobbler::kickstart_default_password')
}
