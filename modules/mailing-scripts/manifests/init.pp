class mailing-scripts {
      cron { 'Rebuilds https://mail.gnome.org/archives':
              command => '/home/admin/mhonarc/update-master',
              user    => 'mailman',
              hour    => '23',
              minute  => '59',
      }

      cron { 'Rebuilds (gnome.org, cvs.gnome.org) aliases':
              command => '/home/admin/bin/mail/export-mail.py',
              user    => 'root',
              hour    => '*',
              minute  => '20'
      }

      cron { 'Updates ClamAV DB virus definitions':
              command => '/usr/bin/freshclam --quiet',
              user    => 'root',
              minute  => '3',
      }
}
