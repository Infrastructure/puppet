#!/usr/bin/python

"""Remove an email address or regexp to ban_list for all lists.

Save as bin/remove_banned.py

Run via

   bin/withlist -a -r remove_banned -- <address_to_unban>

where <address_to_unban> is the actual email address or regexp
to be removed to ban_list for all lists.
"""

def remove_banned(mlist, address):
    if not mlist.Locked():
        mlist.Lock()
    if address in mlist.ban_list:
	mlist.ban_list.remove(address)
    mlist.Save()
    mlist.Unlock()
