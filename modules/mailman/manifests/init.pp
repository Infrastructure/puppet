class mailman {

    $subscribe_form_secret = hiera('mailman::subscribe_form_secret')

    package { 'mailman':
            ensure => installed;
    }

    package { 'python-dns':
            ensure => installed;
    }

    service { 'mailman':
            ensure    => running,
            hasstatus => true,
            require   => Package['mailman'],
    }

    file { '/usr/lib/mailman/Mailman/mm_cfg.py':
            owner   => 'root',
            group   => 'mailman',
            mode    => '0644',
            content => template('mailman/mm_cfg.py'),
            notify  => Service['mailman'],
    }

    file { '/etc/mailman/mm_cfg.py':
            ensure => 'link',
            target => '/usr/lib/mailman/Mailman/mm_cfg.py',
    }

    file { '/etc/mailman/sitelist.cfg':
            owner  => 'mailman',
            group  => 'mailman',
            mode   => '0644',
            source => 'puppet:///modules/mailman/sitelist.cfg',
            notify => Service['mailman'],
    }

    file { '/usr/lib/mailman/bin/add_banned.py':
            owner  => 'root',
            group  => 'mailman',
            mode   => '2755',
            source => 'puppet:///modules/mailman/scripts/add_banned.py',
            notify => Service['mailman'],
    }

    file { '/usr/lib/mailman/bin/remove_banned.py':
            owner  => 'root',
            group  => 'mailman',
            mode   => '2755',
            source => 'puppet:///modules/mailman/scripts/remove_banned.py',
            notify => Service['mailman'],
    }

    file { '/usr/local/bin/newsubstats':
            owner  => 'root',
            group  => 'root',
            mode   => '0750',
            source => 'puppet:///modules/mailman/scripts/newsubstats',
    }

    cron { 'newsubstats':
            command => '/usr/local/bin/newsubstats',
            user    => root,
            hour    => [0],
            minute  => [0],
    }
}
