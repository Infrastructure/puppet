class github::close_pull_requests {

    package {
        'PyYAML': ensure => present;
        'python2-simplejson': ensure => present;
        'python2-pygithub': ensure => present;
    }

    cron { 'Automatically close open GitHub pull requests':
              command => '/home/admin/bin/close_pull_requests.py',
              user    => 'sysadmin',
              hour    => '0',
              minute  => '3',
    }
}
