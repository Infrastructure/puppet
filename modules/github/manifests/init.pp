class github {

    file { '/etc/ssh/github_key':
        source => "puppet:///certificates/github/github_key",
        owner => root,
        group => root,
        mode => 0644,
    }

    file { '/etc/ssh/ssh_known_hosts':
        source => "puppet:///modules/github/ssh_known_hosts",
        owner => root,
        group => root,
        mode => 0644,
    }

    file { '/etc/ssh/ssh_config':
        source => "puppet:///modules/github/ssh_config",
        owner => root,
        group => root,
        mode => 0644,
    }
}

