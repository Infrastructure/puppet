define iptables::firewall (
        $tcpPorts = [],
        $udpPorts = [],
        $custom = [],
        $precustom = [],
        $raw = [],
        $ipv4only = true,
        $custom6 = [],
        $precustom6 = [],
        $raw6 = [],
        $nat = [],
        $intNics = [ 'eth1' ]
){

        package { 'iptables': ensure => installed }

        file { '/etc/sysconfig/iptables':
            content => template('iptables/ip4tables.erb'),
            notify  => Exec['ip4tables-restore'],
        }

        if versioncmp($::operatingsystemrelease, '7') > 0 {
            $iptables_service = 'iptables-services'

            package { 'iptables-services':
                    ensure => present,
            }

            service { 'firewalld':
                    ensure => stopped,
                    enable => false,
            }
        } else {
            $iptables_service = 'iptables'
        }

        service { 'iptables':
            ensure    => running,
            hasstatus => true,
            require   => Package["${iptables_service}"],
            enable    => true,
        }

        if ! ($ipv4only) {
            file { '/etc/sysconfig/ip6tables':
                content => template('iptables/ip6tables.erb'),
                notify  => Exec['ip6tables-restore'],
            }

            service { 'ip6tables':
                ensure    => running,
                hasstatus => true,
                require   => Package["${iptables_service}"],
                enable    => true,
            }
        }

        exec { 'ip4tables-restore':
                command     => '/sbin/iptables-restore /etc/sysconfig/iptables',
                refreshonly => true
        }

        exec { 'ip6tables-restore':
                command     => '/sbin/ip6tables-restore /etc/sysconfig/ip6tables',
                refreshonly => true
        }
}
