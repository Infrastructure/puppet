class httpd::geoip ($behind_proxy = false) {

    include httpd

    validate_bool($behind_proxy)

    package {
        'mod_geoip': ensure => present;
    }

    file { 'geoip.conf':
            ensure  => present,
            path    => '/etc/httpd/conf.d/geoip.conf',
            mode    => '0644',
            owner   => 'root',
            group   => 'root',
            content => template('httpd/geoip.conf'),
            require => [ Package['mod_geoip'] ],
            notify  => Service['httpd']
    }

#    cron { 'GeoIP database update':
#            command => '/usr/bin/geoip-lite-update > /dev/null',
#            user    => root,
#            hour    => 1,
#            minute  => 30,
#            weekday => 'monday',
#    }
}
