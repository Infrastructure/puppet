define httpd::vhost($name, $prefix='', $module='', $update='', $is_template=false) {

        if $is_template {

            $mirrorbrain_db_password = hiera('mirrorbrain::database_password')

            file { "/etc/httpd/sites.d/${prefix}${name}.conf":
                    ensure  => present,
                    mode    => '0644',
                    require => [ Package['httpd'] ],
                    content => template("httpd/sites.d/${name}.conf.erb"),
                    notify  => Service['httpd']
            }
        } else {
            file { "/etc/httpd/sites.d/${prefix}${name}.conf":
                    ensure  => present,
                    mode    => '0644',
                    require => [ Package['httpd'] ],
                    source  => "puppet:///modules/httpd/sites.d/${name}.conf",
                    notify  => Service['httpd']
            }
        }

        file { "/srv/http/${name}":
                ensure  => directory,
                mode    => '0775',
                owner   => 'apache',
                group   => 'apache',
                seltype => 'httpd_sys_content_t',
                require => [ Package['httpd'], File['/srv/http'] ]
        }

        file { "/var/log/httpd/${name}":
                ensure  => directory,
                mode    => '0755',
                require => [ Package['httpd'], File['/var/log/httpd'] ]
        }

        case $update {
                'gnomeweb': { gnomeweb::module { "${module}": name => $module } }

                default:    { }
    }
}
