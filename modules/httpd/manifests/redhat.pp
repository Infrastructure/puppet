class httpd::redhat {
        if $::httpd::use_scl {
                include ::scl

                $httpd_package = 'httpd24'
                $httpd_service = 'httpd24-httpd'
                $mod_ssl_package = 'httpd24-mod_ssl'

                file { '/etc/httpd':
                        ensure => link,
                        force  => true,
                        target => '/opt/rh/httpd24/root/etc/httpd'
                }

                file { ['/var/www', '/var/www/html']:
                        ensure =>  directory
                }
        } else {
                $httpd_package = 'httpd'
                $httpd_service = 'httpd'
                $mod_ssl_package = 'mod_ssl'
        }

        include httpd::reqtimeout

        package { 'httpd':
                        ensure => present,
                        name   => $httpd_package,
        }

        package { 'crypto-utils':
                          ensure => present,
        }

        service { 'httpd':
                ensure     => running,
                name       => $httpd_service,
                enable     => true,
                hasrestart => true,
                hasstatus  => true,
                require    => Package['httpd'],
        }

        file {
                '/etc/httpd/sites.d':
                        ensure  => directory,
                        recurse => true,
                        purge   => true,
                        force   => true,
                        owner   => 'root',
                        group   => 'root',
                        mode    => '0644',
                        source  => 'puppet:///modules/httpd/sites.d-empty',
                        notify  => Service['httpd'];

                'httpd-gnome':
                        ensure => present,
                        path   => '/etc/logrotate.d/httpd-gnome',
                        mode   => '0644',
                        owner  => root,
                        group  => root,
                        source => 'puppet:///modules/httpd/httpd-gnome',
                        require => [ Package['logrotate'] ];

                'welcome.conf':
                        ensure  => present,
                        content => '',
                        path    => '/etc/httpd/conf.d/welcome.conf',
                        notify  => Service['httpd'];

                'httpd.conf':
                        ensure  => present,
                        path    => '/etc/httpd/conf/httpd.conf',
                        mode    => '0644',
                        owner   => 'root',
                        group   => 'root',
                        content => template('httpd/httpd.conf'),
                        require => [ Package['httpd'], File['/etc/httpd/sites.d'] ],
                        notify  => Service['httpd'];

                '/var/log/httpd':
                        ensure => directory,
                        mode   => '0750',
                        owner  => 'root',
                        group  => 'gnomeweb',
                        require => Package['httpd'];

                # logrotate's missingok doesn't work properly for the glob /var/log/httpd/*/*log
                # So we need to create a dummy subdirectory or the entire rotation process
                # will break if we don't have any site-specific log files.
                # https://bugzilla.redhat.com/show_bug.cgi?id=540119
                '/var/log/httpd/dummy-for-logrotate':
                        ensure => directory,
                        mode   => '0750',
                        owner  => 'root',
                        group  => 'gnomeweb',
                        require => File['/var/log/httpd'];

                # Apache modules which automatically make themselves accessible via
                # some alias. The configuration should be done in a vhost only

                '/etc/httpd/conf.d/drupal.conf':
                        ensure => absent;
        }

        file { '/etc/logrotate.d/apache-logs-01':
                  owner  => 'root',
                  group  => 'root',
                  mode   => '0644',
                  source => 'puppet:///modules/httpd/logrotate.d/apache-logs-logrotate-01',
        }

        file { '/etc/logrotate.d/apache-logs-02':
                  owner  => 'root',
                  group  => 'root',
                  mode   => '0644',
                  source => 'puppet:///modules/httpd/logrotate.d/apache-logs-logrotate-02',
        }

        package { 'mod_ssl':
                ensure => present,
                name   => $mod_ssl_package,
        }

        realize(Package['openssl'])

        file {
            'ssl.conf':
                    ensure  => present,
                    path    => '/etc/httpd/conf.d/ssl.conf',
                    mode    => '0644',
                    owner   => 'root',
                    group   => 'root',
                    content => template('httpd/ssl.conf'),
                    require => [ Package[$mod_ssl_package] ],
                    notify  => Service['httpd'];

            '/etc/pki/tls/letsencrypt.intermediate.ca.pem':
                    ensure  => present,
                    mode    => '0644',
                    owner   => 'root',
                    group   => 'apache',
                    source  => 'puppet:///certificates/httpd/letsencrypt.intermediate.ca.pem',
                    require => Package['openssl'],
                    notify  => Service['httpd'];
        }
}
