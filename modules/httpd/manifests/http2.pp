class httpd::http2 {
    include httpd

    file { 'http2.conf':
        ensure => present,
        path   => '/etc/httpd/conf.d/http2.conf',
        mode   => '0644',
        owner  => 'root',
        group  => 'root',
        source => 'puppet:///modules/httpd/http2.conf',
        notify => Service['httpd']
    }
}
