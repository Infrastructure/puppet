class httpd::reqtimeout () {

    include httpd

    file { 'reqtimeout.conf':
            ensure  => present,
            path    => '/etc/httpd/conf.d/reqtimeout.conf',
            mode    => '0644',
            owner   => 'root',
            group   => 'root',
            source  => 'puppet:///modules/httpd/reqtimeout.conf',
            notify  => Service['httpd']
    }
}
