class httpd (
        $use_scl = false,
) {

        case $operatingsystem {
                'Ubuntu':   { include httpd::ubuntu }
                default:    { include httpd::redhat }
        }

        file { '/usr/local/bin/list-abusers':
                owner  => 'root',
                group  => 'root',
                target => '/home/admin/bin/gnomeweb/list-abusers',
        }

        file { '/usr/local/bin/ipl':
                owner  => 'root',
                group  => 'root',
                target => '/home/admin/bin/gnomeweb/list-abusers',
        }

        file { '/srv/http':
                ensure  => directory,
                mode    => '0755',
        }
}
