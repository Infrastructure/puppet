class httpd::ubuntu {

        package { 'apache2':
                ensure => present,
        }

        package { 'openssl':
                ensure => latest,
        }

        service { 'apache2':
                ensure     => running,
                enable     => true,
                hasrestart => true,
                hasstatus  => true,
                require    => Package['apache2']
        }

        file { '/etc/apache2/sites-available':
                ensure  => directory,
                recurse => true,
                purge   => true,
                force   => true,
                owner   => 'root',
                group   => 'root',
                mode    => '0644',
                source  => 'puppet:///modules/httpd/sites-available-empty',
                notify  => Service['apache2'];
        }

        file { '/etc/apache2/sites-enabled':
                ensure  => directory,
                recurse => true,
                purge   => true,
                force   => true,
                owner   => 'root',
                group   => 'root',
                mode    => '0644',
                source  => 'puppet:///modules/httpd/sites-enabled-empty',
                notify  => Service['apache2'];
        }

        file { '/var/log/apache2':
                ensure  => directory,
                mode    => '2750',
                owner   => 'root',
                group   => 'www-data',
                require => Package['apache2']
        }

        file { '/etc/ssl/gnome':
                ensure => directory,
                mode   => '0755',
                owner  => 'root',
                group  => 'root',
        }

        file { '/etc/ssl/gnome/certs':
                ensure => directory,
                mode   => '0755',
                owner  => 'root',
                group  => 'root',
        }

        file { '/etc/ssl/gnome/private':
                ensure => directory,
                mode   => '0600',
                owner  => 'root',
                group  => 'root',
        }

        file { '/etc/ssl/gnome/letsencrypt.intermediate.ca.pem':
                ensure  => present,
                mode    => '0644',
                owner   => 'root',
                group   => 'root',
                source  => 'puppet:///certificates/httpd/letsencrypt.intermediate.ca.pem',
                require => Package['openssl'],
                notify  => Service['apache2'];
        }

        if $::operatingsystem == 'Ubuntu' and $::operatingsystemrelease > '16' {
                $logrotate_user  = 'syslog'
                $logrotate_group = 'adm'
        } elsif $::operatingsystem == 'Debian' {
                $logrotate_user  = 'root'
                $logrotate_group = 'adm'
        }

        file { '/etc/logrotate.d/apache-vhosts':
                ensure  => present,
                mode    => '0644',
                owner   => root,
                group   => root,
                content => template('httpd/logrotate.d/apache-vhosts.erb'),
                require => [ Package['logrotate'] ]
        }

        file { '/etc/logrotate.d/apache-log':
                ensure  => present,
                mode    => '0644',
                owner   => root,
                group   => root,
                content => template('httpd/logrotate.d/apache-log.erb'),
                require => [ Package['logrotate'] ]
        }

        file { '/etc/logrotate.d/apache-error-log':
                ensure  => present,
                mode    => '0644',
                owner   => root,
                group   => root,
                content => template('httpd/logrotate.d/apache-error-log.erb'),
                require => [ Package['logrotate'] ]
        }
}
