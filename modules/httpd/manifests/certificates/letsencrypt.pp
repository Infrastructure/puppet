define httpd::certificates::letsencrypt($name, $ssl_cert_name, $requires_chain=false) {

        case $::operatingsystem {
                'Ubuntu':    {
                    $certs_dir   = '/etc/ssl/gnome'
                    $apache_user = 'www-data'
                    $apache_bin  = 'apache2'
                }
                'Debian':    {
                    $certs_dir   = '/etc/ssl/gnome'
                    $apache_user = 'www-data'
                    $apache_bin  = 'apache2'
                }
                'RedHat':    {
                    $certs_dir   = '/etc/pki/tls'
                    $apache_user = 'apache'
                    $apache_bin  = 'httpd'
                }
                'CentOS':    {
                    $certs_dir   = '/etc/pki/tls'
                    $apache_user = 'apache'
                    $apache_bin  = 'httpd'
                }
        }

        if $requires_chain {

            file { "${certs_dir}/certs/${name}.crt":
                ensure  => present,
                mode    => '0644',
                owner   => 'root',
                group   => "${apache_user}",
                source  => "puppet:///letsencrypt/requires_chain/${ssl_cert_name}.full.crt",
                require => Package['openssl'],
                notify  => Service["${apache_bin}"];
            }

        } else {

            file { "${certs_dir}/certs/${name}.crt":
                ensure  => present,
                mode    => '0644',
                owner   => 'root',
                group   => "${apache_user}",
                source  => "puppet:///letsencrypt/crts/${ssl_cert_name}.crt",
                require => Package['openssl'],
                notify  => Service["${apache_bin}"];
            }
        }

        file { "${certs_dir}/private/${name}.key":
            ensure  => present,
            mode    => '0600',
            owner   => 'root',
            group   => "${apache_user}",
            source  => "puppet:///letsencrypt/keys/${ssl_cert_name}.key",
            require => Package['openssl'],
            notify  => Service["${apache_bin}"];
        }
}
