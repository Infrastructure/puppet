define httpd::ubuntu_vhost($name, $prefix="") {
        
        file { "/etc/apache2/sites-enabled/${prefix}${name}.conf":
                ensure  => present,
                mode    => '0644',
                require => [ Package['apache2'] ],
                source  => "puppet:///modules/httpd/sites-enabled/${name}.conf",
                notify  => Service['apache2']
        }

        file { "/var/log/apache2/${name}":
                ensure  => directory,
                mode    => '0755',
                require => [ Package['apache2'], File['/var/log/apache2'] ]
        }

        include acl
        acl::group::acl_group { "${name}_var_log_apache2_r":
                id   => 'gnomeweb',
                mode => "r-x",
                path => "/var/log/apache2",
        }

        acl::gdefault::acl_gdefault { "var_log_apache2_${name}_default_r":
                id   => 'gnomeweb',
                mode => "r-x",
                path => "/var/log/apache2/${name}",
        }

        acl::group::acl_group { "var_log_apache2_${name}_r":
                id   => 'gnomeweb',
                mode => "r-x",
                path => "/var/log/apache2/${name}",
        }
}
