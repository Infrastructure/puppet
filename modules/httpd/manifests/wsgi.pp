class httpd::wsgi {
    include httpd

    package {
        'mod_wsgi': ensure => present;
    }

    file { 'wsgi.conf':
            ensure  => present,
            path    => '/etc/httpd/conf.d/wsgi.conf',
            mode    => '0644',
            owner   => 'root',
            group   => 'root',
            source  => 'puppet:///modules/httpd/wsgi.conf',
            require => [ Package['mod_wsgi'] ],
            notify  => Service['httpd']
    }
}
