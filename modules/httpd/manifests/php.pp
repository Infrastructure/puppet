class httpd::php {

    package { [
            'php',
            'php-common',
            'php-mysqlnd',
            'php-gd',
            'php-mbstring',
            'php-xml',
            'php-process',
            'php-ldap',
            'php-mcrypt',
            'php-pdo',
            'php-markdown',
            'php-tcpdf'
            ]: ensure => installed,
    }
}

