class rspamd {
    include ::clamav

    yumrepo { 'rspamd':
        baseurl       => "http://rspamd.com/rpm-stable/centos-7/x86_64/",
        enabled       => 1,
        gpgcheck      => 1,
        repo_gpgcheck => 1,
        gpgkey        => 'http://rspamd.com/rpm/gpg.key',
    }

    package { 'redis':
        ensure => installed,
    }

    package { 'rspamd':
        ensure  => installed,
        require => [ Yumrepo['rspamd'], Service['redis'] ]
    }

    file { "/etc/rspamd/rspamd.conf.local":
        mode    => '0644',
        require => [ Package['rspamd'] ],
        source  => "puppet:///modules/rspamd/rspamd.conf.local",
        recurse => true,
        notify  => Service['rspamd'],
    }

    file { "/etc/rspamd/local.d":
        ensure  => directory,
        mode    => '0644',
        require => [ Package['rspamd'] ],
        source  => "puppet:///modules/rspamd/local.d",
        recurse => true,
        notify  => Service['rspamd'],
    }

    service { 'rspamd':
        ensure    => running,
        enable    => true,
        hasstatus => true,
        require   => Package['rspamd'],
    }

    service { 'redis':
        ensure    => running,
        enable    => true,
        hasstatus => true,
        require   => Package['redis'],
    }
}
