#!/usr/bin/ruby

require 'yaml'
require 'pp'
require 'optparse'
require 'rubygems'

# We need this to interpret the report file (the report file contains serialised
# puppet objects)
require 'puppet'


# Map the return codes to numbers
RET = {
    'UNKOWN'   => 3,
    'CRITICAL' => 2,
    'WARNING'  => 1,
    'OK'       => 0,
}
# Where we find the various files
STATE_LOC      = '/var/lib/puppet/state'
SUMMARY_LOC    = "#{STATE_LOC}/last_run_summary.yaml"
REPORT_LOC     = "#{STATE_LOC}/last_run_report.yaml"
#    Puppet 2.x lock files
LOCK_LOC       = "#{STATE_LOC}/puppetdlock"
#    Puppet 3.x lock files
ADMIN_LOCK_LOC = "#{STATE_LOC}/agent_disabled.lock"
AGENT_LOCK_LOC = "#{STATE_LOC}/agent_catalog_run.lock"

# How we want to react to the different events
# list them all so that we can give the stats.
types = {
  'changed'           => 'OK',  # c
  'failed'            => 'OK',  # f
  'failed_to_restart' => 'OK',  # g
  'out_of_sync'       => 'OK',  # o
  'restarted'         => 'OK',  # r
  'scheduled'         => 'OK',  # s
  'skipped'           => 'OK',  # t
  'total'             => 'OK',
}
lock_check        = 'OK'   # l
lock_age_limit    = 259200
summary_age_limit = 259200

ARGV.options do |opts|
  opts.banner = "Usage: puppet_state [options]"
  opts.on("-c", "--changed-warn", "Warning on changed resources") do
    types['changed'] = 'WARNING'
  end
  opts.on("-C", "--changed-crit", "Critical on changed resources") do
    types['changed'] = 'CRITICAL'
  end
  opts.on("-f", "--failed-warn", "Warning on failed resources") do
    types['failed'] = 'WARNING'
  end
  opts.on("-F", "--failed-crit", "Critical on failed resources") do
    types['failed'] = 'CRITICAL'
  end
  opts.on("-g", "--failed-restart-warn", "Warning on failed_restart resources") do
    types['failed_to_restart'] = 'WARNING'
  end
  opts.on("-G", "--failed-restart-crit", "Critical on failed_restart resources") do
    types['failed_to_restart'] = 'CRITICAL'
  end
  opts.on("-o", "--sync-warn", "Warning on out of sync resources") do
    types['out_of_sync'] = 'WARNING'
  end
  opts.on("-O", "--sync-crit", "Critical on out of sync resources") do
    types['out_of_sync'] = 'CRITICAL'
  end
  opts.on("-r", "--restarted-warn", "Warning on restarted resources") do
    types['restarted'] = 'WARNING'
  end
  opts.on("-R", "--restarted-crit", "Critical on restarted resources") do
    types['restarted'] = 'CRITICAL'
  end
  opts.on("-s", "--scheduled-warn", "Warning on scheduled resources") do
    types['scheduled'] = 'WARNING'
  end
  opts.on("-S", "--scheduled-crit", "Critical on scheduled resources") do
    types['scheduled'] = 'CRITICAL'
  end
  opts.on("-t", "--skipped-warn", "Warning on skipped resources") do
    types['skipped'] = 'WARNING'
  end
  opts.on("-T", "--skipped-crit", "Critical on skipped resources") do
    types['skipped'] = 'CRITICAL'
  end
  opts.on("-l", "--lock-warn", "Warning on old lock files") do
    lock_check = 'WARNING'
  end
  opts.on("-L", "--lock-crit", "Critical on old lock files") do
    lock_check = 'CRITICAL'
  end
  opts.on("-m", "--lock-age AGE", "Critical on old lock files") do |age|
    lock_age_limit = age
  end
  opts.on("-M", "--summary-age AGE", "Critical on old summary file") do |age|
    summary_age_limit = age
  end
  opts.parse!
end


$current_state = 'OK'
$current_notes = []
$current_stats = []

def update_state(state, note, stat)
  if RET[state] > RET[$current_state]
    $current_state = state
    if note
      $current_notes.unshift(note)
    end
    if stat
      $current_stats.unshift(stat)
    end
  else
    if note
      $current_notes.push(note)
    end
    if stat
      $current_stats.push(stat)
    end
  end
end

begin
  # checks for existence and YAML validity
  summary = YAML.load_file(SUMMARY_LOC)

  # WARN if puppet's not run at all for the last 36 hours
  mtime = File.stat(SUMMARY_LOC).mtime
  now = Time.now
  summary_age = (now - mtime).to_i
  if summary_age > summary_age_limit.to_i
    update_state('WARNING', "Summary file #{summary_age} seconds old (Puppet not running successfully)", "summary_age=#{summary_age}")
  else
    update_state('OK', nil, "summary_age=#{summary_age}")
  end

  # Check the lock files
  begin
    stat = File.stat(LOCK_LOC)
    if stat
      age = (now - stat.mtime).to_i
      if lock_check != 'OK' && age > lock_age_limit
        update_state(lock_check, "Puppet Lock file #{age} seconds old", "lock_age=#{age}")
      else
        update_state('OK', nil, "lock_age=#{age}")
      end
    end
  rescue
  end
  begin
    stat = File.stat(AGENT_LOCK_LOC)
    if stat
      age = (now - stat.mtime).to_i
      if lock_check != 'OK' && age > lock_age_limit
        update_state(lock_check, "Puppet Agent Lock file #{age} seconds old", "agent_lock_age=#{age}")
      else
        update_state('OK', nil, "agent_lock_age=#{age}")
      end
    end
  rescue
  end
  begin
    stat = File.stat(ADMIN_LOCK_LOC)
    if stat
      age = (now - stat.mtime).to_i
      if lock_check != 'OK' && age > lock_age_limit
        update_state(lock_check, "Puppet Administrative Lock file #{age} seconds old", "admin_lock_age=#{age}")
      else
        update_state('OK', nil, "admin_lock_age=#{age}")
      end
    end
  rescue
  end

  if summary.has_key? 'resources'
    types.sort.each do |type, level|
      if summary['resources'].has_key?type
        count = summary['resources'][type]
        if count > 0 && types[type] != 'OK'
          update_state(types[type], "#{type} resource(s)", "#{type}=#{count}")
        else
          update_state('OK', nil, "#{type}=#{count}")
        end
      end
    end
  else
    begin
      report = YAML.load_file(REPORT_LOC)
      message = report.logs[0].message
    rescue Exception => e
      message = 'UNKNOWN CAUSE (catalog compilation probably failed)'
    end
    update_state('CRITICAL', "Puppet run failed : #{message}", nil)
  end

rescue Exception => e
  update_state('UNKOWN', e.message, nil)
end


all_notes = $current_notes.join(',')
all_stats = $current_stats.join(';')
print "#{$current_state}: #{all_notes} | #{all_stats}\n";
exit(RET[$current_state])
