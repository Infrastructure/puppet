class nrpe::packages {

  if ($::operatingsystem == 'RedHat' or $::operatingsystem == 'CentOS') {
    package { [
              'nrpe',
              'nagios-plugins-disk',
              'nagios-plugins-users',
              'nagios-plugins-swap',
              'nagios-plugins-load',
              'nagios-plugins-ping',
              'nagios-plugins-procs',
              'nagios-plugins-http',
              'nagios-plugins-ntp',
              'nagios-plugins-ssh',
              'nagios-plugins-mysql',
              'nagios-plugins-nrpe',
              'pynag',
              ]: ensure => installed,
    }
  } else {
    package { 'nagios-plugins':
        ensure => present
    }

    package { 'nagios-nrpe-server':
        ensure => present
    }
  }
}
