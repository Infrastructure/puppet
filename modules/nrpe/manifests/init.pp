class nrpe (
  $custom_plugins = hiera_hash('nrpe::custom_plugins', {})
) {

    include nrpe::vars
    include nrpe::packages

    $libdir = $nrpe::vars::libdir

    service { "${nrpe::vars::service_name}":
            ensure  => running,
            enable  => true,
            require => Package["${nrpe::vars::package_name}"]
    }

    file { '/etc/nagios/nrpe.cfg':
            content => template('nrpe/nrpe.cfg.erb'),
            mode    => '0644',
            notify  => Service["${nrpe::vars::service_name}"],
            require => Package["${nrpe::vars::package_name}"]
    }

    file { '/etc/nrpe.d':
            ensure  => directory,
            recurse => true,
            purge   => true,
            force   => true,
            owner   => 'root',
            group   => 'root',
            mode    => '0644',
            notify  => Service["${nrpe::vars::service_name}"];
    }

    if ($::operatingsystem == "Debian" or $::operatingsystem == "Ubuntu") {
        file { '/var/run/nagios':
                ensure => directory,
                owner  => "${nrpe::vars::run_as}",
                group  => "${nrpe::vars::run_as}",
                mode   => '0755',
        }
    }

    file { '/etc/nrpe.d/nrpe_local.cfg':
            content => template('nrpe/nrpe_local.cfg.erb'),
            mode    => '0644',
            notify  => Service["${nrpe::vars::service_name}"],
            require => Package["${nrpe::vars::package_name}"]
    }

    if $::manufacturer == 'HP' {
      sudo::extra{ 'HP Acu Cli Sudoers rule': name => 'hpacucli' }

      nrpe::plugin { 'check_hpacucli':
        plugin => 'check_hpacucli.py',
      }
    }

    if $::manufacturer == 'Dell Inc.' {
      nrpe::plugin { 'check_openmanage':
        args => '-H localhost',
      }
    }

    if ( ! empty($custom_plugins)) {
      create_resources('nrpe::plugin', $custom_plugins)
    }

    # Default custom plugins
    nrpe::plugin { 'check_puppet_state':
      plugin         => 'check_puppet_state.rb',
      args           => '-M 10800',
      command_prefix => 'USE_DEFAULTS',
      external       => true,
    }

    if $datacenter == 'RDU2' {
      $ntp_host = 'guido.osci.io'
    } else {
      $ntp_host = '1.rhel.pool.ntp.org'
    }

    nrpe::plugin { 'check_ntp_time':
      plugin         => 'check_ntp_time',
      args           => "-H ${ntp_host} -w 0.1 -c 1",
      external       => false,
    }
}
