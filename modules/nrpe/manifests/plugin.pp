# == Define: nrpe::plugin
define nrpe::plugin (
  $ensure           = 'present',
  $args             = 'UNSET',
  $plugin           = 'USE_DEFAULTS',
  $command_prefix   = 'UNSET',
  $pkg_dependencies = {},
  $external         = false,
) {

  validate_re($ensure,'^(present)|(absent)$',
    "nrpe::plugin::${name}::ensure must be 'present' or 'absent'. Detected value is <${ensure}>.")

  validate_bool($external)

  if $ensure == 'present' {
    $plugin_ensure = 'file'
  } else {
    $plugin_ensure = 'absent'
  }

  if $plugin == 'USE_DEFAULTS' {
    $plugin_real = $name
  } else {
    $plugin_real = $plugin
  }

  include nrpe::vars
  $libdir = "/usr/${nrpe::vars::libdir}/nagios/plugins"

  if $command_prefix == 'USE_DEFAULTS' {
    $command_prefix_real = $nrpe::vars::command_prefix
  } elsif $command_prefix == 'UNSET' {
    $command_prefix_real = $command_prefix
  } else {
    $command_prefix_real = $command_prefix
    validate_absolute_path($command_prefix_real)
  }

  if ( ! empty($pkg_dependencies)) {
    create_resources(package, $pkg_dependencies)
  }

  if $external {
    unless File["${libdir}/${plugin_real}"] {
      file { "${libdir}/${plugin_real}":
        ensure  => file,
        content => template("nrpe/plugins/${plugin_real}"),
        owner   => 'root',
        group   => 'root',
        mode    => '0755',
      }
    }
  }

  file { "nrpe_plugin_${name}":
    ensure  => $plugin_ensure,
    path    => "/etc/nrpe.d/${name}.cfg",
    content => template('nrpe/plugin.erb'),
    owner   => "${nrpe::vars::run_as}",
    group   => "${nrpe::vars::run_as}",
    mode    => '0640',
    require => [ File['/etc/nagios/nrpe.cfg'], Package["${nrpe::vars::package_name}"] ],
    notify  => Service["${nrpe::vars::service_name}"]
  }
}
