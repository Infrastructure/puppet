class nrpe::vars {

    $command_prefix = '/usr/bin/sudo'

    if ($::operatingsystem == 'RedHat' or $::operatingsystem == 'CentOS') {
      $service_name = 'nrpe'
      $package_name = 'nrpe'
      $run_as = 'nrpe'

      if ($::architecture == 'x86_64' or $::architecture == 'aarch64') {
        $libdir = 'lib64'
      } else {
        $libdir = 'lib'
      }
    } else {
      $service_name = 'nagios-nrpe-server'
      $package_name = 'nagios-nrpe-server'
      $run_as = 'nagios'
      $libdir = 'lib'
    }
}
