#######################
## MANAGED BY PUPPET ##
#######################

# Misc special-purpose aliases

root@tomboy-online.com gnome-sysadmin@gnome.org
abuse@tomboy-online.com gnome-sysadmin@gnome.org
hostmaster@tomboy-online.com gnome-sysadmin@gnome.org
postmaster@tomboy-online.com gnome-sysadmin@gnome.org
webmaster@tomboy-online.com gnome-web-list@gnome.org
