define postfix::postmapfile($name, $is_template=false) {

    exec { "postmap_${name}":
        command     => "/usr/sbin/postmap /etc/postfix/${name}",
        refreshonly => true,
        require     => [
                    File["/etc/postfix/${name}"],
                    Package['postfix']],
    }

    if ($is_template) {
        file { "/etc/postfix/${name}":
            ensure  => present,
            mode    => '0644',
            content => template("postfix/${name}.erb"),
            notify  => Exec["postmap_${name}"],
        }
    } else {
        file { "/etc/postfix/${name}":
            ensure => present,
            mode   => '0644',
            source => "puppet:///modules/postfix/${name}",
            notify => Exec["postmap_${name}"],
        }
    }
}
