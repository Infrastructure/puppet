class postfix::client (
    $custom_inet_interfaces = undef,
) inherits postfix::base {

    $postfix_relayhost = 'smtp-int.gnome.org'

    File['main.cf'] {
        content => template('postfix/main.cf')
    }

    mailalias
    { 'root':
        ensure    => present,
        target    => '/etc/aliases',
        recipient => 'gnome-sysadmin@gnome.org',
        notify    => Exec['newaliases']
    }
}
