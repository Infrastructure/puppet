class postfix::server inherits postfix::base {

    file { '/etc/pki/tls/certs/smtp.gnome.org.crt':
        ensure => file,
        mode   => '0644',
        owner  => 'root',
        group  => 'root',
        notify => Service['postfix'],
        source => 'puppet:///letsencrypt/requires_chain/gnome.org.full.crt',
    }

    file { '/etc/pki/tls/private/smtp.gnome.org.key':
        ensure => file,
        mode   => '0600',
        owner  => 'root',
        group  => 'root',
        notify => Service['postfix'],
        source => 'puppet:///letsencrypt/keys/gnome.org.key',
    }

    exec { '/etc/postfix/dh2048.pem':
        command => '/usr/bin/openssl dhparam -out /etc/postfix/dh2048.pem 2048',
        creates => '/etc/postfix/dh2048.pem',
        notify  => Service['postfix'],
        before  => File['main.cf'],
    }

    exec { '/etc/postfix/dh512.pem':
        command => '/usr/bin/openssl dhparam -out /etc/postfix/dh512.pem 512',
        creates => '/etc/postfix/dh512.pem',
        notify  => Service['postfix'],
        before  => File['main.cf'],
    }

    mailalias
        { 'moderator':
                ensure    => present,
                target    => '/etc/aliases',
                recipient => ':include:/var/mailman/list-of-moderators',
                notify    => Exec['newaliases']
        }

    $postfix_custom = [
        'header_checks = regexp:/etc/postfix/header_checks',
        'mime_header_checks = regexp:/etc/postfix/mime_header_checks',
        'body_checks = regexp:/etc/postfix/body_checks',

        # generic server stuff:
        'smtpd_recipient_limit = 4096',
        'default_process_limit = 200',
        'maximal_backoff_time = 1000s',
        'minimal_backoff_time = 300s',
        'default_destination_concurrency_limit = 50',
        'smtp_destination_concurrency_limit = 10',

        # Keep mails for 1 day in the queue before dropping
        # all them completely.
        'maximal_queue_lifetime = 1d',

        # Bump attachment's size to 25MB.
        'message_size_limit = 25480000',

        # Optimize for lists by serializing deliveries
        'qmgr_fudge_factor = 100',

        # staff-mail
        'virtual_transport = lmtp:unix:private/dovecot-lmtp',
        'mailbox_transport = lmtp:unix:private/dovecot-lmtp',

        # XXX move to node level
        'transport_maps = hash:/etc/postfix/transport',

        # 2011-07-23: ovitters: disable SASL, heavily used by spammers, don't think
        # we still have legitimate users
        # XXX following is all smtp.gnome.org specific, should be moved to node config level!
        # 'smtpd_sasl_auth_enable = yes',
        # 'smtpd_sasl_security_options = noanonymous',
        # 'broken_sasl_auth_clients = yes',

        # for testing purposes ONLY, not to be used because the
        # current LDAP schema design/abuse is b0rk city
        # 'ldap_aliases_server_host = account.gnome.org',
        # 'ldap_aliases_search_base = cn=users,cn=accounts,dc=gnome,dc=org',
        # 'ldap_aliases_query_filter = (uid=%u)',
        # 'ldap_aliases_result_attribute = mail',

        # rspamd
        'smtpd_milters = inet:127.0.0.1:11332',
        'milter_default_action = accept',

        # XXX seems sane for default server config, I guess
        'smtpd_delay_reject = no',
        'smtp_tls_security_level = may',
        'smtp_tls_CAfile = /etc/pki/tls/certs/ca-bundle.crt',
        'smtp_tls_policy_maps = hash:/etc/postfix/tls_policy_per_domain',
        'smtpd_restriction_classes = check_recipient_bounce',
        'check_recipient_bounce = check_recipient_access hash:/etc/postfix/access-recipient-bounce',
        'smtpd_client_restrictions = sleep 1, reject_unauth_pipelining',
        'smtpd_helo_restrictions =',
        'smtpd_sender_restrictions =',
        'smtpd_recipient_restrictions =',
        #'    reject_unlisted_sender',
        '    reject_non_fqdn_sender',
        '    reject_non_fqdn_recipient',
        '    reject_unknown_sender_domain',
        '    reject_unknown_recipient_domain',
        '    check_client_access hash:/etc/postfix/access-client',
        '    check_helo_access hash:/etc/postfix/access-helo',
        '    check_sender_access hash:/etc/postfix/access-sender',
        '    reject_unlisted_recipient',
        '    permit_mynetworks',
        '    check_recipient_access hash:/etc/postfix/access-recipient',
        '    reject_unauth_destination',
        '    reject_invalid_hostname',
        '    reject_non_fqdn_hostname',
        '    reject_unknown_reverse_client_hostname',
        '    reject_unauth_pipelining',
        '    check_client_access hash:/etc/postfix/access-rbl-exception-client',
        '    permit',


        'smtpd_tls_eecdh_grade = strong',
        'smtpd_tls_dh1024_param_file = /etc/postfix/dh2048.pem',
        'smtpd_tls_dh512_param_file = /etc/postfix/dh512.pem',

        'recipient_delimiter = +',
    ]

    File['main.cf'] {
        content => template('postfix/main.cf')
    }

    file {
        'body_checks':
            path   => '/etc/postfix/body_checks',
            mode   => '0644',
            source => 'puppet:///modules/postfix/body_checks'
    }

    file { 'header_checks':
            path   => '/etc/postfix/header_checks',
            mode   => '0644',
            source => 'puppet:///modules/postfix/header_checks'
    }

    file { 'mime_header_checks':
            path   => '/etc/postfix/mime_header_checks',
            mode   => '0644',
            source => 'puppet:///modules/postfix/mime_header_checks'
    }

    file { '/etc/postfix/virtual-aliases':
                ensure => 'directory',
                owner  => 'root',
                group  => 'root',
                mode   => '0644',
    }

    postfix::postmapfile { 'access-client': name => 'access-client' }
    postfix::postmapfile { 'access-helo': name => 'access-helo' }
    postfix::postmapfile { 'access-rbl-exception-client': name => 'access-rbl-exception-client' }
    postfix::postmapfile { 'access-sender': name => 'access-sender' }
    postfix::postmapfile { 'access-recipient': name => 'access-recipient' }
    postfix::postmapfile { 'access-recipient-bounce': name => 'access-recipient-bounce' }
    postfix::postmapfile { 'tls_policy_per_domain': name => 'tls_policy_per_domain' }
}
