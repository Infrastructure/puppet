class postfix::base {

    if $::operatingsystem == 'RedHat' or $::operatingsystem == 'CentOS' {
        $ca_bundle_path = '/etc/pki/tls/certs/ca-bundle.crt'
        $libexec_path   = '/usr/libexec/postfix'
    } elsif $::operatingsystem == 'Ubuntu' {
        $ca_bundle_path = '/etc/ssl/certs/ca-certificates.crt'
        $libexec_path   = '/usr/lib/postfix/sbin'
    } else {
        $ca_bundle_path = '/etc/ssl/certs/ca-certificates.crt'
        $libexec_path   = '/usr/lib/postfix'
    }

    package {
        'postfix':
            ensure => present;

        'procmail':
            ensure => present;
    }

    service { 'postfix':
        ensure     => running,
        enable     => true,
        hasrestart => true,
        hasstatus  => true,
        require    => Package['postfix']
    }

    file { 'main.cf':
        ensure  => 'present',
        path    => '/etc/postfix/main.cf',
        mode    => '0644',
        owner   => 'root',
        group   => 'root',
        require => Package['postfix'],
        notify  => Service['postfix']
    }

    file { '/etc/aliases':
        ensure => 'present',
    }

    exec { 'newaliases':
        path        => ['/usr/bin', '/usr/sbin'],
        subscribe   => File['/etc/aliases'],
        refreshonly => true,
    }
}
