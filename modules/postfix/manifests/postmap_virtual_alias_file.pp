define postfix::postmap_virtual_alias_file($name) {
        exec { "postmap-${name}":
                command     => "/usr/sbin/postmap /etc/postfix/virtual-aliases/${name}",
                refreshonly => true,
                require     => [
                            File["/etc/postfix/virtual-aliases/${name}"],
                            Package['postfix']],
        }

        exec { "postfix-reload-${name}":
                    command     => '/sbin/service postfix reload',
                    refreshonly => true,
                    require     => [
                            File["/etc/postfix/virtual-aliases/${name}"],
                            Package['postfix']],
                    subscribe   => File["/etc/postfix/virtual-aliases/${name}"],
        }

        file { "/etc/postfix/virtual-aliases/${name}":
                ensure => 'present',
                mode   => '0644',
                source => "puppet:///modules/postfix/virtual-aliases/${name}",
                notify => Exec["postmap-${name}"]
        }
}
