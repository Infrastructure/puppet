define postfix::postaliasfile($name) {
    exec { "postalias_${name}":
        command     => "/usr/sbin/postalias /etc/postfix/${name}",
        refreshonly => true,
        require     => [
                    File["/etc/postfix/${name}"],
                    Package['postfix']],
    }

    file { "/etc/postfix/${name}":
        ensure => present,
        mode   => '0644',
        source => "puppet:///modules/postfix/${name}",
        notify => Exec["postalias${name}"]
    }
}
