class selinux::rsyslogd_auditd {
        include selinux
        
        selmodule { 'gnome_internal_syslogd_auditd':
                ensure       => present,
                selmoduledir => '/usr/share/selinux/custom/gnome',
                syncversion  => true
        }
}
