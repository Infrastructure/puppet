class selinux::puppetmaster {
        include selinux
        
        selmodule { 'gnome_internal_puppetmaster':
                ensure       => present,
                selmoduledir => '/usr/share/selinux/custom/gnome',
                syncversion  => true
        }
}
