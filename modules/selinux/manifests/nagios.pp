class selinux::nagios {
        include selinux
        
        selmodule { 'gnome_internal_nagios':
                ensure       => present,
                selmoduledir => '/usr/share/selinux/custom/gnome',
                syncversion  => true
        }
}
