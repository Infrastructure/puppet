class selinux::bugzilla {
        include selinux
        
        selmodule { 'gnome_internal_bugzilla':
                ensure       => present,
                selmoduledir => '/usr/share/selinux/custom/gnome',
                syncversion  => true
        }
}
