class selinux::procmail {
        include selinux
        
        selmodule { 'gnome_internal_procmail':
                ensure       => present,
                selmoduledir => '/usr/share/selinux/custom/gnome',
                syncversion  => true
        }
}
