class selinux::postfix {
        include selinux
        
        selmodule { 'gnome_internal_postfix':
                ensure       => present,
                selmoduledir => '/usr/share/selinux/custom/gnome',
                syncversion  => true
        }
}
