class selinux::sshd {
        include selinux
        
        selmodule { 'gnome_internal_sshd':
                ensure       => present,
                selmoduledir => '/usr/share/selinux/custom/gnome',
                syncversion  => true
        }
}
