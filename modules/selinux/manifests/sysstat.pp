class selinux::sysstat {
        include selinux
        
        selmodule { 'gnome_internal_sysstat':
                ensure       => present,
                selmoduledir => '/usr/share/selinux/custom/gnome',
                syncversion  => true
        }
}
