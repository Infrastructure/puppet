class selinux::timedatectl_puppet {
        include selinux
        
        selmodule { 'gnome_internal_timedatectl_puppet':
                ensure       => present,
                selmoduledir => '/usr/share/selinux/custom/gnome',
                syncversion  => true
        }
}
