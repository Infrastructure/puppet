class mock {
        package {
              'mock': ensure => present;
        }

        # Cache builds to make rebuilds faster
        package {
              'ccache': ensure => present;
        }

        # For inspecting packages with yumdownloader
        package {
              'yum-utils': ensure => present;
        }
        # Apparently EL6's mock doesn't pull this in
        package {
              'rpm-build': ensure => present;
        }

        file { 'default.cfg':
                ensure  => present,
                path    => '/etc/mock/default.cfg',
                mode    => '0644',
                owner   => root,
                group   => root,
                require => Package['mock'],
                source  => 'puppet:///modules/mock/default.cfg'
        }
        file { 'epel-6-x86_64.cfg':
                ensure  => present,
                path    => '/etc/mock/epel-6-x86_64.cfg',
                mode    => '0644',
                owner   => root,
                group   => root,
                require => Package['mock'],
                source  => 'puppet:///modules/mock/epel-6-x86_64.cfg'
        }
        file { 'epel-7-x86_64.cfg':
                ensure  => present,
                path    => '/etc/mock/epel-7-x86_64.cfg',
                mode    => '0644',
                owner   => root,
                group   => root,
                require => Package['mock'],
                source  => 'puppet:///modules/mock/epel-7-x86_64.cfg'
        }
        # Ensure that chroots don't have to be initialized after every build
        file { 'site-defaults.cfg':
                ensure  => present,
                path    => '/etc/mock/site-defaults.cfg',
                mode    => '0644',
                owner   => root,
                group   => root,
                require => Package['mock'],
                source  => 'puppet:///modules/mock/site-defaults.cfg'
        }
}
