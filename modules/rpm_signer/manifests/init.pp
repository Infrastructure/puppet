class rpm_signer {

        file { '/srv/rpm-repo-keys':
                ensure  => directory,
                owner   => 'sysadmin',
                group   => 'sysadmin',
                recurse => true,
                mode    => '0700',
        }

        file { '/usr/local/bin/rpm-signer':
                ensure => link,
                target => '/home/admin/bin/rpm-signer.py',
        }

        file { '/srv/rpm-signer.config':
                ensure => present,
                mode   => '0664',
                owner  => 'sysadmin',
                group  => 'sysadmin',
                source => 'puppet:///modules/rpm_signer/rpm-signer.config'
        }
}
