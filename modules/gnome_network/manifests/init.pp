class gnome_network {

        if (!$datacenter) {
            $datacenter = 'PHX2'
        }

        if $::fqdn =~ /^oscp\-(master|router|node)/ {

            package { 'dnsmasq': ensure => present }

            service { 'dnsmasq':
                    ensure     => running,
                    enable     => true,
                    hasrestart => true,
                    hasstatus  => true,
                    require    => Package['dnsmasq'],
            }

            file { '/etc/hosts':
                        ensure  => present,
                        path    => '/etc/hosts',
                        mode    => '0644',
                        owner   => root,
                        group   => root,
                        content => template('gnome_network/hosts_oscp')
            }

            file { '/etc/dnsmasq.d/origin-dns.conf':
                        ensure  => present,
                        mode    => '0600',
                        owner   => root,
                        group   => root,
                        content => template('gnome_network/dnsmasq/origin-dns.conf'),
                        require => Package['dnsmasq'],
                        notify  => Service['dnsmasq'],
            }

            file { '/etc/dnsmasq.d/origin-upstream-dns.conf':
                        ensure  => present,
                        mode    => '0600',
                        owner   => root,
                        group   => root,
                        content => template('gnome_network/dnsmasq/origin-upstream-dns.conf'),
                        require => Package['dnsmasq'],
                        notify  => Service['dnsmasq'],
            }
        } else {

            file { '/etc/hosts':
                        ensure  => present,
                        mode    => '0644',
                        owner   => root,
                        group   => root,
                        content => template('gnome_network/hosts')
            }

            file { '/etc/resolv.conf':
                        ensure  => present,
                        mode    => '0644',
                        owner   => root,
                        group   => root,
                        content => template('gnome_network/resolv.conf')
            }
        }
}
