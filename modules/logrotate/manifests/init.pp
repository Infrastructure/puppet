class logrotate {

  package { 'logrotate':
              ensure => present
  }
  
  file { '/etc/logrotate.conf':
          ensure  => present,
          mode    => '0644',
          owner   => root,
          group   => root,
          source  => 'puppet:///modules/logrotate/logrotate.conf',
          require => [ Package['logrotate'] ]
  }
}
