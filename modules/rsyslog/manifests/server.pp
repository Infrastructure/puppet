class rsyslog::server {
        
          package { 'rsyslog':  ensure => installed }

          service {'rsyslog':
                  ensure     => running,
                  enable     => true,
                  hasstatus  => true,
                  hasrestart => true,
                  require    => Package[rsyslog],
          }
          
          file { '/etc/rsyslog.conf':
                  owner  => 'root',
                  group  => 'root',
                  mode   => '0644',
                  source => 'puppet:///modules/rsyslog/rsyslog.conf',
                  notify => Service['rsyslog'],
          }

          file { '/etc/logrotate.d/var-log-hosts':
                  owner  => 'root',
                  group  => 'root',
                  mode   => '0644',
                  source => 'puppet:///modules/rsyslog/var-log-hosts',
          }
}
