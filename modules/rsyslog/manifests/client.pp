class rsyslog::client {

          include selinux::rsyslogd_auditd

          package {
            'rsyslog':  ensure => installed;
            'sysklogd': ensure => absent, require => Package['rsyslog'];
          }

          service {'rsyslog':
                  ensure     => running,
                  enable     => true,
                  hasstatus  => true,
                  hasrestart => true,
                  require    => Package[rsyslog],
          }

          file { '/etc/rsyslog.conf':
                  owner   => 'root',
                  group   => 'root',
                  mode    => '0644',
                  content => template("rsyslog/rsyslog.conf.erb"),
                  notify  => Service['rsyslog'],
          }

          file { '/var/spool/rsyslog':
                  ensure => directory,
                  owner  => 'root',
                  group  => 'root',
                  mode   => '0750',
          }
}
