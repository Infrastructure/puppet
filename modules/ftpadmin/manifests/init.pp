class ftpadmin {

    package {
        'xz':
            ensure => present;
    }

    if $::operatingsystemrelease =~ /^6\./ {
        package {
            'python-argparse':
                ensure => present;

            'pyliblzma':
                ensure => present;
        }
    }

    file {
        '/etc/rsyncd.conf':
            ensure => 'present',
            mode   => '0644',
            owner  => 'root',
            group  => 'root',
            source => 'puppet:///modules/ftpadmin/rsyncd.conf';

        'install-module':
            ensure => '/home/admin/bin/install-module-alias',
            owner  => 'root',
            group  => 'root',
            path   => '/usr/local/bin/install-module';

        'ftpadmin':
            ensure => '/home/admin/bin/ftpadmin',
            owner  => 'root',
            group  => 'root',
            path   => '/usr/local/bin/ftpadmin';

        'signal-ftp-sync':
            ensure => '/home/admin/bin/signal-ftp-sync',
            owner  => 'root',
            group  => 'root',
            path   => '/usr/local/bin/signal-ftp-sync';

        '/home/gnomeftp':
            ensure => directory,
            owner  => 'gnomeftp',
            group  => 'gnomeftp',
            mode   => '0700';

        '/home/gnomeftp/.ssh':
            ensure  => directory,
            owner   => 'gnomeftp',
            group   => 'gnomeftp',
            mode    => '0700',
            require => File['/home/gnomeftp'];

        '/home/gnomeftp/.ssh/known_hosts':
            ensure  => present,
            owner   => gnomeftp,
            group   => gnomeftp,
            mode    => '0600',
            source  => 'puppet:///modules/ftpadmin/known_hosts',
            require => File['/home/gnomeftp/.ssh'];
    }

    sudo::extra{ 'FTPAdmin Sudoers file': name => 'master' }

    xinetd::enable { 'rsync':
        service => rsync,
        require => [ Package['rsync'], Package['xinetd'] ]
    }
}
