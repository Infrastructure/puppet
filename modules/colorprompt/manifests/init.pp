# A Puppet class for coloring the Bash prompt
class colorprompt {
    file { 'color_prompt.sh':
        ensure => present,
        path   => '/etc/profile.d/color_prompt.sh',
        mode   => '0755',
        owner  => 'root',
        group  => 'root',
        source => 'puppet:///modules/colorprompt/color_prompt.sh',
    }
}
