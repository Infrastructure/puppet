class baseclass {
        if ($::operatingsystem == 'RedHat' or $::operatingsystem == 'CentOS') {
                include packages::epel
        }

        if (!$datacenter) {
            $datacenter = 'RDU2'
        }

        if ($datacenter == 'OSUOSL') {
            augeas { "Don't manage DNS via NM":
                    context => '/files/etc/NetworkManager/NetworkManager.conf',
                    changes => [
                            'set main/dns none',
                    ],
            }
        }

        if $::ipaddress_br1 =~ /^172.31.2/ {
          $InternalNic = 'br1'
        } elsif $::ipaddress_br2 =~ /^172.31.2/ {
          $InternalNic = 'br2'
        } else {
          $InternalNic = 'eth1'
        }

        if !($::ipaddress =~ /^8.43.85/ or $::ipaddress_eth0 or $::ipaddress_br1) {
            if $datacenter == 'RDU2' {
                augeas { 'set_default_gateway':
                        context => "/files/etc/sysconfig/network-scripts/ifcfg-${InternalNic}",
                        changes => [
                                'set GATEWAY 172.31.2.28',
                                ],
                }
            }
        } else {
                augeas { 'remove_default_gateway':
                        context => "/files/etc/sysconfig/network-scripts/ifcfg-${InternalNic}",
                        changes => [
                                'rm GATEWAY',
                                ],
                }
        }

        if ($::operatingsystem == 'RedHat' or $::operatingsystem == 'CentOS') and $::lsbmajdistrelease >= '7' {
                include timezone::rhel7
        } else {
                include timezone::utc
        }

        class { 'gnome_users':
            create_auth_via_glusterfs => false
        }

        # SELinux GNOME rules coming from the
        # gnome-selinux package on the gnome-repo.
        include selinux::nagios
        include selinux::postfix
        include selinux::sysstat
        include selinux::sshd

        package { 'setools-console':
                ensure => present,
        }

        package { 'policycoreutils-python':
                ensure => present,
        }

        package { 'emacs-nox':
                ensure => present,
        }

        package { 'vim-enhanced':
                ensure => present,
        }

        package { 'redhat-lsb':
                ensure => present,
        }

        package { 'git':
                ensure => present,
        }

        package { 'python36-ldap':
                ensure => present,
        }

        @package {'openssl':
                ensure => latest,
        }

        include gnome_network
        include logrotate
        include colorprompt
        include logwatch
        include xinetd::base
        include nrpe
        include abrt
        include chrony

        if $::fqdn !~ /^puppet(master(0[0-9])|).gnome.org/ {
                include puppet::client
        }

        include sudo
        sudo::extra{ 'check_puppet_state sudoers rule': name => 'check_puppet_state' }

        # Needed by check_puppet_state.rb
        package { 'rubygems':
                ensure => present,
        }

        if str2bool("${::selinux}") {
            selboolean { 'nagios_run_sudo':
                    name       => 'nagios_run_sudo',
                    persistent => true,
                    value      => on
            }
        }

        include motd
        include packages::gnome
        include packages::autoupdate
        include packages::disablerhn
        include ksu

        if ($::ipaddress =~ /^8.43.85/ or $::ipaddress_eth0 or $::ipaddress_br1) {
            include denyhosts
        }

        file { '/root/.bashrc':
                owner  => 'root',
                group  => 'root',
                source => 'puppet:///modules/baseclass/bashrc',
        }

        if ($::operatingsystem == 'RedHat' or $::operatingsystem == 'CentOS') {

            if ($::fqdn !~ /^oscp\-(master|router|node)/) {
                file { '/etc/yum.conf':
                        content => template('baseclass/yum.conf.erb'),
                        owner   => root,
                        group   => root,
                        mode    => '0644',
                }
            }

            if $yum_proxy_enabled {
                if $datacenter == 'RDU2' {
                    $yum_proxy_hostname = 'bastion.gnome.org'
                }

                augeas { 'yum_proxy_enabled':
                        context => '/files/etc/rhsm/rhsm.conf',
                        changes => [
                                "set server/proxy_hostname ${yum_proxy_hostname}",
                                'set server/proxy_port 3128',
                                ],
                }
            } else {
                augeas { 'yum_proxy_disabled':
                        context => '/files/etc/rhsm/rhsm.conf',
                        changes => [
                                'rm server/proxy_hostname',
                                'rm server/proxy_port'
                                ],
                }
            }
        }

        service { 'avahi-daemon':
            ensure => 'stopped',
            enable => false,
        }

        service { 'cups':
            ensure => 'stopped',
            enable => false,
        }

        service { 'cups.path':
            ensure => 'stopped',
            enable => false,
        }

        service { 'cups.socket':
            ensure => 'stopped',
            enable => false,
        }

        $root_password = hiera('root::password')
        user { 'root':
                password => "${root_password}"
        }

        if ($::fqdn =~ /^oscp\-(master|router|node)/) {
            include openshift::tuning::journald
        }

        if (($datacenter == 'RDU2') and ($::ipaddress_tun0 != '10.8.0.1')) {
            include openvpn::routes
        }
}
