# Class to define config files for the sudo command

class sudo {
    package {
        'sudo':
            ensure => present;
    }

    case $::operatingsystem {

    'redhat', 'centos': {
        file { 'sudoers':
            ensure  => present,
            path    => '/etc/sudoers',
            mode    => '0440',
            owner   => 'root',
            group   => 'root',
            require => Package[sudo],
            content => template('sudo/sudoers.redhat');
        }
      }
    'debian', 'ubuntu': {
        file { 'sudoers':
            ensure  => present,
            path    => '/etc/sudoers',
            mode    => '0440',
            owner   => 'root',
            group   => 'root',
            require => Package[sudo],
            content => template('sudo/sudoers.debian');
        }
      }

    default: {
        fail("Module ${module_name} is not supported on ${::operatingsystem}")
      }
    }

    file {
        '/etc/sudoers.d':
            ensure  => directory,
            recurse => true,
            purge   => true,
            force   => true,
            owner   => 'root',
            group   => 'root',
            # This mode will also apply to files from the source directory
            # puppet will automatically set +x for directories
            mode    => '0440',
            source  => 'puppet:///modules/sudo/sudoers.d-empty',
            require => Package[sudo];
    }
}  
