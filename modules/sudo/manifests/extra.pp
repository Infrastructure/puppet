# Type defined to add extra config files to allow specific users
# to be able to sudo as a different user.
define sudo::extra($name) {
        
    file {
        "/etc/sudoers.d/${name}":
            ensure  => present,
            mode    => '0440',
            content => template("sudo/sudoers.d/${name}"),
            require => Package['sudo'];
    }
}
