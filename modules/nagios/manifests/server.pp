class nagios::server {

        $av_htaccess = hiera('nagios::av_password')
        $bpiotrowski_htaccess = hiera('nagios::bpiotrowski_password')
        $cisco_imc_nagios_password = hiera('nagios::cisco_imc_nagios_password')
        $gitlab_rw_token = hiera('nagios::gitlab_rw_token')

        package { [
                'nagios',
                'nagios-plugins-all',
                'perl-libwww-perl',
                'nmap-ncat',
                'python-Cisco-IMC-SDK',
                'perl-LWP-Protocol-https',
                'python2-gitlab',
                'python2-dateutil',
                'pytz'
                ]:
                ensure => installed,
        }

        service {'nagios':
                ensure     => running,
                enable     => true,
                hasstatus  => true,
                hasrestart => true,
                require    => Package['nagios'],
        }

        file { '/etc/nagios/conf.d':
                ensure  => directory,
                recurse => true,
                purge   => true,
                force   => true,
                owner   => 'root',
                group   => 'nagios',
                mode    => '0644',
                source  => 'puppet:///modules/nagios/nagios',
                notify  => Service['nagios'];
        }

        file { '/usr/local/bin/pagerduty_nagios.pl':
                owner  => 'root',
                group  => 'root',
                mode   => '0755',
                source => 'puppet:///modules/nagios/scripts/pagerduty_nagios.pl',
        }

        file { '/etc/nagios/passwd':
                owner   => 'root',
                group   => 'apache',
                mode    => '0660',
                content => template('nagios/apache/passwd.erb'),
                notify  => Service['nagios'],
        }

        cron { 'pagerduty':
                command => '/usr/local/bin/pagerduty_nagios.pl flush',
                user    => nagios,
                minute  => '*/2',
                require => File['/usr/local/bin/pagerduty_nagios.pl'],
        }

        file { '/etc/nagios/conf.d/commands.cfg':
                owner   => 'root',
                group   => 'nagios',
                mode    => '0644',
                content => template('nagios/nagios/commands.cfg.erb'),
                require => Package['nagios'],
                notify  => Service['nagios'],
        }

        file { '/etc/nagios/resources.cfg':
                owner   => 'root',
                group   => 'nagios',
                mode    => '0640',
                content => template('nagios/nagios/resources.cfg.erb'),
                require => Package['nagios'],
                notify  => Service['nagios'],
        }

        file { '/etc/nagios/nagios.cfg':
                owner   => 'root',
                group   => 'nagios',
                mode    => '0644',
                content => template('nagios/nagios/nagios.cfg.erb'),
                require => Package['nagios'],
                notify  => Service['nagios'],
        }

        file { '/etc/httpd/conf.d/nagios.conf':
                owner   => 'root',
                group   => 'root',
                mode    => '0644',
                source  => 'puppet:///modules/nagios/apache/nagios.conf',
                require => Package['nagios'],
                notify  => Service['httpd'],
        }

        file { '/etc/nagios/cgi.cfg':
                owner   => 'root',
                group   => 'root',
                mode    => '0644',
                source  => 'puppet:///modules/nagios/apache/cgi.cfg',
                require => Package['nagios'],
                notify  => Service['nagios'],
        }

        file { '/usr/lib64/nagios/plugins/check_proxy':
                owner  => 'root',
                group  => 'root',
                mode   => '0755',
                source => 'puppet:///modules/nagios/scripts/check_proxy',
                notify => Service['nagios'],
        }

        file { '/usr/lib64/nagios/plugins/check_rrsig':
                owner  => 'root',
                group  => 'root',
                mode   => '0755',
                source => 'puppet:///modules/nagios/scripts/check_rrsig',
                notify => Service['nagios'],
        }

        file { '/usr/lib64/nagios/plugins/cisco_imc_nagios':
                owner  => 'root',
                group  => 'root',
                mode   => '0755',
                source => 'puppet:///modules/nagios/scripts/cisco_imc_nagios',
        }

        file { '/usr/lib64/nagios/plugins/cisco_imc_nagios.cfg':
                owner  => 'root',
                group  => 'root',
                mode   => '0644',
                source => 'puppet:///modules/nagios/scripts/cisco_imc_nagios.cfg',
        }

        file { '/usr/local/bin/irc-colorize.py':
                mode   => '0755',
                owner  => root,
                group  => root,
                source => 'puppet:///modules/nagios/scripts/irc-colorize.py'
        }

        file { '/usr/lib64/nagios/plugins/check_gitlab_runner':
                owner  => 'root',
                group  => 'root',
                mode   => '0755',
                source => 'puppet:///modules/nagios/scripts/check_gitlab_runner',
                notify => Service['nagios'],
        }

        file { '/home/admin/secret/gitlab_rw':
                ensure  => present,
                mode    => '0644',
                owner   => root,
                group   => root,
                content => "GITLAB_PRIVATE_RW_TOKEN=${gitlab_rw_token}",
                require => File['/home/admin/secret'],
        }
}
