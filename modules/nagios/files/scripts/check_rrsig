#!/bin/bash

## examples:
## check_rrsig -n nsig2.hauke-lampe.de -z hauke-lampe.de -w +1week
## check_rrsig -n parent.rfc5011.shinkuro.com -z roll_one-31.rfc5011.shinkuro.com -w +2days

DEBUG=

function usage {
        echo "Usage: $0 -n <ns> -z <zone> [ -w <refresh interval> ]"
}

while getopts n:z:w:d opt; do
        case "$opt" in
                "n")    server=${OPTARG};;
                "z")    zone=${OPTARG};;
                "w")    refreshwarn=${OPTARG};;
                "d")    DEBUG=1;;
                "*")    echo "Invalid option: ${OPTARG}"; usage; exit 3;;
        esac
done;

[ -z "$server" -o -z "$zone" ] && {
	echo "Missing nameserver or zone"
	usage
        exit 3
};

[ -z "$refreshwarn" ] && refreshwarn="+5days"

NOW="`date -u "+%Y%m%d%H%M%S"`"
SOON="`date -u "+%Y%m%d%H%M%S" -d "$refreshwarn"`"

response="`dig +noall +answer +norec +dnssec -t rrsig +cd "$zone" @$server |sed -e 's/^;;.*\$//'`"
[ -n "$DEBUG" ] && echo "RESPONSE: $response";
[ -z "$response" ] && { echo "RRSIG WARNING: Empty DNS response"; exit 3; }


timestamps="`echo "$response"|sed -e 's/^[a-zA-Z0-9][^ \t]*[ \t]\t*[0-9]*[ \t]\t*IN[ \t]\t*RRSIG[ \t]\t*[A-Z]* [0-9]* [0-9]* [0-9]* \([0-9]*\) .*/\1/'`"
[ -n "$DEBUG" ] && echo "TIMESTAMPS: $timestamps";
[ -z "$timestamps" ] && { echo "RRSIG WARNING: No signature timestamps found"; exit 3; }

minexpire=""
maxexpire=""

#timestamps=20090101234212

for ts in $timestamps; do
        [ -n "$DEBUG" ] && echo "TS: $ts";
	[ -z "$minexpire" ] && minexpire="$ts"
	[ -z "$maxexpire" ] && maxexpire="$ts"
	[ "$minexpire" -gt "$ts" ] && minexpire="$ts"
	[ "$maxexpire" -lt "$ts" ] && maxexpire="$ts"
done

[ -z "$minexpire" ] && { echo "RRSIG WARNING: minexpire undefined"; exit 3; }

[ "$minexpire" -lt "$NOW" ] && {
	echo "RRSIG CRITICAL: Expired signature for $zone: $minexpire < $NOW (max: $maxexpire)"; 
	exit 2;
}

[ "$minexpire" -lt "$SOON" ] && {
	echo "RRSIG WARNING: Signatures expire soon on $zone: $minexpire < $SOON (max: $maxexpire)"; 
	exit 1;
}

echo "RRSIG OK: Valid signature timestamps for $zone (min: $minexpire / max: $maxexpire)"
exit 0;

