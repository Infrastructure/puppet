class virsh {

    file { "/etc/sysconfig/libvirt-guests":
         owner   => "root",
         group   => "root",
         mode    => 0640,
         source => "puppet:///modules/virsh/libvirt-guests",
    }
}
