define nginx::certificates::letsencrypt($name, $ssl_cert_name, $requires_chain=false) {
    if $requires_chain {

        file { "/etc/pki/tls/certs/${name}.crt":
            ensure  => present,
            mode    => '0644',
            owner   => 'root',
            group   => "nginx",
            source  => "puppet:///letsencrypt/requires_chain/${ssl_cert_name}.full.crt",
            require => Package['openssl'],
            notify  => Service["nginx"];
        }

    } else {

        file { "/etc/pki/tls/certs/${name}.crt":
            ensure  => present,
            mode    => '0644',
            owner   => 'root',
            group   => "nginx",
            source  => "puppet:///letsencrypt/crts/${ssl_cert_name}.crt",
            require => Package['openssl'],
            notify  => Service["nginx"];
        }
    }

    file { "/etc/pki/tls/private/${name}.key":
        ensure  => present,
        mode    => '0600',
        owner   => 'root',
        group   => "nginx",
        source  => "puppet:///letsencrypt/keys/${ssl_cert_name}.key",
        require => Package['openssl'],
        notify  => Service["nginx"];
    }
}
