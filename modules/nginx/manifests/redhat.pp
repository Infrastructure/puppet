class nginx::redhat {
    realize(Package['openssl'])

    package { 'nginx':
        ensure => present,
        name   => 'nginx',
    }

    service { 'nginx':
        ensure     => running,
        name       => 'nginx',
        enable     => true,
        hasrestart => true,
        hasstatus  => true,
        require    => Package['nginx'],
    }

    file { "/etc/nginx/nginx.conf":
        ensure  => present,
        mode    => '0644',
        require => [ Package['nginx'] ],
        source  => "puppet:///modules/nginx/nginx.conf",
        notify  => Service['nginx']
    }
}
