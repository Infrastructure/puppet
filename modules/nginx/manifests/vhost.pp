define nginx::vhost($name, $prefix='', $is_template=false) {

    file { "/etc/nginx/snippets.d":
        ensure  => directory,
        mode    => '0644',
        require => [ Package['nginx'] ],
        source  => "puppet:///modules/nginx/snippets.d/${name}",
        recurse => true,
        notify  => Service['nginx']
    }

    if $is_template {
        file { "/etc/nginx/conf.d/${prefix}${name}.conf":
                ensure  => present,
                mode    => '0644',
                require => [ Package['nginx'] ],
                content => template("nginx/conf.d/${name}.conf.erb"),
                notify  => Service['nginx']
        }
    } else {
        file { "/etc/nginx/conf.d/${prefix}${name}.conf":
                ensure  => present,
                mode    => '0644',
                require => [ Package['nginx'] ],
                source  => "puppet:///modules/nginx/conf.d/${name}.conf",
                notify  => Service['nginx']
        }
    }
}
