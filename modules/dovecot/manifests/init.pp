class dovecot (
    $ssl           = false,
    $ssl_key_path  = undef,
    $ssl_cert_path = undef,

    $maildir        = '/srv/dovecot',
    $default_fields = '%d/%u',

    $ldap_hosts            = undef,
    $ldap_auth_bind_userdn = undef,
    $ldap_tls_ca_cert_file = undef,
    $ldap_base             = undef,
    $ldap_user_attrs       = undef,
    $ldap_user_filter      = undef,
    $ldap_pass_attrs       = undef,
    $ldap_pass_filter      = undef,
    $ldap_iterate_attrs    = undef,
    $ldap_iterate_filter   = undef,
    $mail_privileged_group = undef,
) {
    package {
        'dovecot': ensure => present;
        'dovecot-pigeonhole': ensure => present;
    }

    service { 'dovecot':
        ensure  => running,
        enable  => true,
        require => Package['dovecot', 'dovecot-pigeonhole']
    }

    $homedir = "${maildir}/${default_fields}"

    file { "${maildir}":
        ensure  => 'directory',
        owner   => 'dovecot',
        group   => 'staffmail',
        mode    => '0770',
        seltype => 'mail_home_rw_t',
    }

    file { '/etc/dovecot/dovecot.conf':
      ensure  => 'file',
      content => template('dovecot/dovecot.conf.erb'),
      require => Package[ 'dovecot', 'dovecot-pigeonhole' ],
      notify  => Service[ 'dovecot' ],
    }

    file { '/etc/dovecot/conf.d/auth-ldap.conf.ext':
      ensure  => 'file',
      content => template('dovecot/auth-ldap.conf.ext.erb'),
      require => Package[ 'dovecot', 'dovecot-pigeonhole' ],
      notify  => Service[ 'dovecot' ],
    }

    file { '/etc/dovecot/dovecot-ldap.conf.ext':
      ensure  => 'file',
      content => template('dovecot/dovecot-ldap.conf.ext.erb'),
      require => Package[ 'dovecot', 'dovecot-pigeonhole' ],
      notify  => Service[ 'dovecot' ],
    }

    file { '/etc/dovecot/conf.d/20-lmtp.conf':
      ensure  => 'file',
      content => template('dovecot/20-lmtp.conf.erb'),
      require => Package[ 'dovecot', 'dovecot-pigeonhole' ],
      notify  => Service[ 'dovecot' ],
    }

    file { '/etc/dovecot/conf.d/20-managesieve.conf':
      ensure  => 'file',
      content => template('dovecot/20-managesieve.conf.erb'),
      require => Package[ 'dovecot', 'dovecot-pigeonhole' ],
      notify  => Service[ 'dovecot' ],
    }

    file { '/etc/dovecot/conf.d/10-auth.conf':
      ensure  => 'file',
      content => template('dovecot/10-auth.conf.erb'),
      require => Package[ 'dovecot', 'dovecot-pigeonhole' ],
      notify  => Service[ 'dovecot' ],
    }

    file { '/etc/dovecot/conf.d/10-mail.conf':
      ensure  => 'file',
      content => template('dovecot/10-mail.conf.erb'),
      require => Package[ 'dovecot', 'dovecot-pigeonhole' ],
      notify  => Service[ 'dovecot' ],
    }

    file { '/etc/dovecot/conf.d/10-master.conf':
      ensure  => 'file',
      content => template('dovecot/10-master.conf.erb'),
      require => Package[ 'dovecot', 'dovecot-pigeonhole' ],
      notify  => Service[ 'dovecot' ],
    }

    file { '/etc/dovecot/conf.d/10-ssl.conf':
      ensure  => 'file',
      content => template('dovecot/10-ssl.conf.erb'),
      require => Package[ 'dovecot', 'dovecot-pigeonhole' ],
      notify  => Service[ 'dovecot' ],
    }

    file { "${ssl_cert_path}":
        ensure => file,
        mode   => '0644',
        owner  => 'root',
        group  => 'root',
        notify => Service['dovecot'],
        source => 'puppet:///letsencrypt/requires_chain/gnome.org.full.crt',
    }

    file { "${ssl_key_path}":
        ensure => file,
        mode   => '0600',
        owner  => 'root',
        group  => 'root',
        notify => Service['dovecot'],
        source => 'puppet:///letsencrypt/keys/gnome.org.key',
    }
}
