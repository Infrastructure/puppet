class glusterfs::server {

    package { 'glusterfs-server':
          ensure => installed;
    }

    service { 'glusterd':
          ensure    => running,
          enable    => true,
          hasstatus => true,
          require   => Package['glusterfs-server'],
    }
}
