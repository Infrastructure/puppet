class glusterfs::client {

    package { 'glusterfs':
          ensure => installed;
    }

    package { 'glusterfs-fuse':
          ensure => installed;
    }

    file { '/home/admin':
            ensure => directory,
            mode   => '0755',
            owner  => sysadmin,
            group  => sysadmin,
    }

    file { '/home/users':
            ensure => directory,
            mode   => '0755',
            owner  => root,
            group  => root,
    }

    if $::operatingsystemrelease =~ /^6\./ {
            $fstype        = 'glusterfs'
            $users_options = 'rw,backupvolfile-server=range-back,_netdev'
            if $::fqdn =~ /^puppet(master(0[0-9])|).gnome.org/ {
                $admin_options = 'rw,backupvolfile-server=range-back,acl,_netdev'
            } else {
                $admin_options = 'ro,backupvolfile-server=range-back,acl,_netdev'
            }
    }
    if $::operatingsystemrelease =~ /^7\./ {
            $fstype        = 'glusterfs'
            $users_options = 'rw,backupvolfile-server=range-back,x-systemd.device-timeout=5min'
            if $::fqdn =~ /^puppet(master(0[0-9])|).gnome.org/ {
                $admin_options = 'rw,backupvolfile-server=range-back,acl,x-systemd.device-timeout=5min'
            } else {
                $admin_options = 'ro,backupvolfile-server=range-back,acl,x-systemd.device-timeout=5min'
            }
    }

    if ! ($freeipa::client::install_oddjobd) {
        mount { '/home/users':
                ensure  => mounted,
                device  => 'accelerator-back:/users',
                fstype  => "${fstype}",
                atboot  => true,
                options => "${users_options}",
                require => Package['glusterfs', 'glusterfs-fuse'],
        }
    }

    mount { '/home/admin':
            ensure  => mounted,
            device  => 'accelerator-back:/admin',
            fstype  => "${fstype}",
            atboot  => true,
            options => "${admin_options}",
            require => Package['glusterfs', 'glusterfs-fuse'],
    }

    if str2bool("${::selinux}") {
            selboolean { 'use_fusefs_home_dirs':
                    name       => 'use_fusefs_home_dirs',
                    persistent => true,
                    value      => on
            }
    }

    case $hostname {
        restaurant: {
            # These file declarations here duplicate files that are owned
            # by the mailman package - added here explicitly since depending
            # on the mailman package in the nfs module would be odd
            file {
                '/var/lib/mailman':
                ensure => directory,
                mode   => '0755',
                owner  => root,
                group  => mailman,
            }

            file {
                '/var/lib/mailman/archives':
                ensure  => directory,
                mode    => '0755',
                owner   => root,
                group   => mailman,
                require => File['/var/lib/mailman']
            }

            mount {
                '/var/lib/mailman/archives':
                ensure  => mounted,
                device  => 'accelerator-back:/mailman',
                fstype  => glusterfs,
                atboot  => true,
                options => 'rw,backupvolfile-server=range-back',
                require => File['/var/lib/mailman/archives']
            }
        }

        master: {
            file { '/ftp': ensure => directory; }
            file { '/gimp_ftp': ensure => directory; }

            mount {
                '/ftp':
                ensure => mounted,
                device => 'scale-back:/ftp',
                atboot => true,
                fstype => glusterfs,
                require => File['/ftp'],
                options => 'rw,backupvolfile-server=gesture-back';
            }

            mount {
                '/gimp_ftp':
                ensure => mounted,
                device => 'scale-back:/oscp_gimp-ftp',
                atboot => true,
                fstype => glusterfs,
                require => File['/gimp_ftp'],
                options => 'rw,backupvolfile-server=gesture-back';
            }
        }
    }
}
