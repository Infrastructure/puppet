class postgresql::check_postgres {
  file { '/usr/local/bin/check_postgres.pl':
    ensure => present,
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
    source => 'puppet:///modules/postgresql/check_postgres.pl'
  }

  sudo::extra { 'check_postgres':
    name => 'check_postgres'
  }

  nrpe::plugin { 'check_postgres_connection':
    plugin   => 'check_postgres',
    args     => '--action=connection --datadir /var/opt/rh/rh-postgresql12/lib/pgsql/data',
    external => true,
  }

  nrpe::plugin { 'check_postgres_delay':
    plugin   => 'check_postgres',
    args     => '-u replicator --action=hot_standby_delay -H 172.31.2.2,172.31.2.1 --datadir /var/opt/rh/rh-postgresql12/lib/pgsql/data --warning="1048576 and 2 min" --critical="16777216 and 10 min"',
    external => true,
  }
}
