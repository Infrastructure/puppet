class postgresql (
        $enable_check_mk = true,
) {

        package { 'postgresql': ensure => installed, }
        package { 'postgresql-server': ensure => installed, }
        package { 'postgresql-contrib': ensure => installed, }

        service { 'postgresql':
                ensure    => running,
                enable    => true,
                hasstatus => true,
        }

        if $enable_check_mk {
                file { '/usr/share/check-mk-agent/plugins/mk_postgres':
                        ensure => link,
                        target => '/usr/share/check-mk-agent/available-plugins/mk_postgres',
                }
        }
}

