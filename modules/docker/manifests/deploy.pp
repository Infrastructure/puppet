class docker::deploy {

        sm_add_repo { 'Enable rhel-7-server-extras-rpms':
            repo_name => 'rhel-7-server-extras-rpms'
        }

        sm_add_repo { 'Enable rhel-7-server-optional-rpms':
            repo_name => 'rhel-7-server-optional-rpms'
        }

        package { 'docker':
                ensure  => installed,
                require => [Sm_add_repo['Enable rhel-7-server-extras-rpms'], Sm_add_repo['Enable rhel-7-server-optional-rpms']]
        }

        service { 'docker':
                ensure    => running,
                hasstatus => true,
                enable    => true,
                require   => Package['docker'],
        }
}
