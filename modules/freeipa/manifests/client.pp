class freeipa::client (
    $is_master = false,
    $is_slave  = false,
    $install_oddjobd = false,
) {

    validate_bool($is_master)
    validate_bool($is_slave)
    validate_bool($install_oddjobd)

    if ($is_master and $is_slave) {
        fail("${::fqdn} can't be both a FreeIPA master and slave")
    }

    if ($::operatingsystem == 'Ubuntu' or $::operatingsystem == 'Debian') {
        package { 'sssd': ensure         => installed }
        package { 'sssd-tools': ensure   => installed }
    }

    if ($::operatingsystem == 'RedHat' or $::operatingsystem == 'CentOS') {
        package { 'ipa-client': ensure => installed }

        package { [ 'sssd', 'sssd-client', 'libtdb', 'libtalloc', 'pam', 'cyrus-sasl-gssapi', 'libtevent', 'libldb', 'libsemanage',
        'libdhash', 'libpath_utils', 'libref_array']:
          ensure => latest,
          notify => Exec['sssd-restart'],
        }
    }

    service {
        'sssd':
                  enable     => true,
                  ensure     => running,
                  hasstatus  => true,
                  hasrestart => true,
                  require    => File['/etc/sssd/sssd.conf'];
    }

    case $::operatingsystem {
        redhat,centos: {
                  $restart = '/sbin/service sssd restart'
                  $reload  = '/sbin/service sssd reload'
        }
        ubuntu,debian: {
                  $restart = '/usr/sbin/service sssd restart'
                  $reload  = '/usr/sbin/service sssd reload'
        }
    }

    exec {
        'sssd-restart':
                  command     => $restart,
                  refreshonly => true,
                  subscribe   => File['/etc/sssd/sssd.conf'];
        'sssd-reload':
                  command     => $reload,
                  refreshonly => true,
                  require     => File['/etc/sssd/sssd.conf'];
    }

    if ($::operatingsystem == 'RedHat' or $::operatingsystem == 'CentOS') and $::operatingsystemrelease =~ /^7\./ {
        file { '/etc/pam.d/password-auth-ac':
              ensure => file,
              owner  => 'root',
              group  => 'root',
              mode   => 0644,
              content => template('freeipa/pam.d/password-auth-RedHat-7'),
        }

        file { '/etc/pam.d/system-auth-ac':
              ensure => file,
              owner  => 'root',
              group  => 'root',
              mode   => 0644,
              content => template('freeipa/pam.d/system-auth-RedHat-7'),
        }

        file { '/etc/pam.d/password-auth':
              ensure => 'link',
              target => '/etc/pam.d/password-auth-ac',
        }

        file { '/etc/pam.d/system-auth':
              ensure => 'link',
              target => '/etc/pam.d/system-auth-ac',
        }

    } elsif ($::operatingsystem == 'RedHat' or $::operatingsystem == 'CentOS') and $::operatingsystemrelease =~ /^6\./ {
        file { '/etc/pam.d/password-auth-ac':
              ensure  => file,
              owner   => 'root',
              group   => 'root',
              mode    => 0644,
              content => template('freeipa/pam.d/password-auth-RedHat-6')
        }

        file { '/etc/pam.d/system-auth-ac':
              ensure  => file,
              owner   => 'root',
              group   => 'root',
              mode    => 0644,
              content => template('freeipa/pam.d/system-auth-RedHat-6')
        }
    }

    file { '/etc/ipa': ensure => directory }

    if ($is_master) {

        file { '/etc/krb5.conf':
            ensure => file,
            owner  => 'root',
            group  => 'root',
            mode   => 0644,
            source => 'puppet:///modules/freeipa/krb/krb5-master.conf',
        }

    } elsif ($is_slave) {

        file { '/etc/krb5.conf':
            ensure => file,
            owner  => 'root',
            group  => 'root',
            mode   => 0644,
            source => 'puppet:///modules/freeipa/krb/krb5-slave.conf';
        }

    } else {

        file { '/etc/krb5.conf':
            ensure  => file,
            owner   => 'root',
            group   => 'root',
            mode    => 0644,
            content => template('freeipa/krb5.conf'),
        }

    }

    if ($is_slave or $is_master) {

        file { '/etc/ipa/ca.crt':
              ensure => file,
              owner  => 'root',
              group  => 'root',
              mode   => 0644,
              source => 'puppet:///certificates/freeipa/ca-master.crt',
        }
    } else {

        file { '/etc/ipa/ca.crt':
              ensure => file,
              owner  => 'root',
              group  => 'root',
              mode   => 0644,
              source => 'puppet:///certificates/freeipa/ca.crt',
              notify => Exec['sssd-restart']
        }
    }

    if ($install_oddjobd) {
        service { 'oddjobd':
          enable     => true,
          ensure     => running,
          hasstatus  => true,
          hasrestart => true,
        }
    }

    file { '/etc/sssd/sssd.conf':
            ensure  => file,
            owner   => 'root',
            group   => 'root',
            mode    => 0600,
            content => template('freeipa/sssd.conf'),
            notify  => Exec['sssd-restart']
    }

    file { '/etc/nsswitch.conf':
            ensure => file,
            owner  => 'root',
            group  => 'root',
            mode   => 0644,
            source => 'puppet:///modules/freeipa/sssd/nsswitch.conf',
    }

    file { '/etc/krb5.keytab':
            ensure  => file,
            owner   => 'root',
            group   => 'root',
            mode    => 0600,
            seltype => 'krb5_keytab_t',
            source  => "puppet:///certificates/keytabs/${::fqdn}.keytab",
    }
}
