class freeipa::server {

    sudo::extra{ 'check_ipa_status': name => 'check_ipa_status' }

    realize(Package['openssl'])

    package { 'ipa-server':
          ensure => present;
    }

    package { 'mod_ssl':
          ensure => present;
    }

    service { 'ipa':
          name      => 'ipa',
          ensure    => running,
          enable    => true,
          hasrestart => true,
          hasstatus  => true,
          require    => Package['ipa-server'];
    }

    service { 'dirsrv@GNOME-ORG':
          name      => 'dirsrv@GNOME-ORG',
          ensure    => running,
          enable    => true,
          hasrestart => true,
          hasstatus  => true,
          require    => Package['ipa-server'];
    }

    service { 'httpd':
          name       => 'httpd',
          ensure     => running,
          enable     => true,
          hasrestart => true,
          hasstatus  => true,
    }

    file { '/etc/dirsrv/slapd-GNOME-ORG/schema/96foundation.ldif':
        ensure => file,
        owner  => 'dirsrv',
        group  => 'dirsrv',
        mode   => 0440,
        source => 'puppet:///modules/freeipa/schemas/96foundation.ldif',
        notify => Service['dirsrv@GNOME-ORG']
    }

    file { '/etc/dirsrv/slapd-GNOME-ORG/schema/95module.ldif':
        ensure => file,
        owner  => 'dirsrv',
        group  => 'dirsrv',
        mode   => 0440,
        source => 'puppet:///modules/freeipa/schemas/95module.ldif',
        notify => Service['dirsrv@GNOME-ORG']
    }

    file { '/etc/dirsrv/slapd-GNOME-ORG/schema/97cvs.ldif':
        ensure => file,
        owner  => 'dirsrv',
        group  => 'dirsrv',
        mode   => 0440,
        source => 'puppet:///modules/freeipa/schemas/97cvs.ldif',
        notify => Service['dirsrv@GNOME-ORG']
    }

    file { '/etc/dirsrv/slapd-GNOME-ORG/schema/98sshd.ldif':
        ensure => file,
        owner  => 'dirsrv',
        group  => 'dirsrv',
        mode   => 0440,
        source => 'puppet:///modules/freeipa/schemas/98sshd.ldif',
        notify => Service['dirsrv@GNOME-ORG']
    }

    file { '/usr/lib/python2.7/site-packages/ipalib/plugins/foundation.py':
        ensure => file,
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        source => 'puppet:///modules/freeipa/foundation/foundation.py',
        notify => Service['ipa']
    }

    file { '/usr/share/ipa/ui/js/plugins/foundation':
        ensure => directory,
        owner  => 'root',
        group  => 'root',
    }

    file { '/usr/share/ipa/ui/js/plugins/foundation/foundation.js':
        ensure => file,
        owner  => 'root',
        group  => 'root',
        source => 'puppet:///modules/freeipa/foundation/foundation.js',
        notify => Service['httpd']
    }

    file { '/etc/httpd/conf.d/ipa-rewrite.conf':
        ensure  => file,
        owner   => 'root',
        group   => 'root',
        mode    => 0644,
        content => template('freeipa/ipa-rewrite.conf'),
        notify  => Service['httpd']
    }

    file { '/etc/httpd/conf.d/ssl.conf':
        ensure  => file,
        owner   => 'root',
        group   => 'root',
        mode    => 0644,
        content => template('freeipa/ssl.conf'),
        notify  => Service['httpd']
    }
}
