from ipalib.plugins import user
from ipalib.parameters import Str
from ipalib import _
from time import strftime
import re

def validate_date(ugettext, value):
    if not re.match("^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$", value):
        return _("The entered date is wrong, please make sure it matches the YYYY-MM-DD syntax")

user.user.takes_params = user.user.takes_params + (
 Str('firstadded?', validate_date,
 cli_name='firstadded',
 label=_('First Added date'),
 ),
 Str('lastrenewedon?', validate_date,
 cli_name='lastrenewedon',
 label=_('Last Renewed on date'),
 ),
)
