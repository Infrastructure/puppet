# A class to manage the installation and configuration of abrt
class abrt {

    package { 'abrt':
        ensure => installed,
    }

    file { '/etc/abrt/abrt-action-save-package-data.conf':
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        source => 'puppet:///modules/abrt/abrt-action-save-package-data.conf',
    }
}
