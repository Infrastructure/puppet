define gitlab_artifacts($project=undef, $namespace='GNOME', $branch='master', $job_name=undef, $extracted_path=undef, $unzip_user_exec='apache', $extr_dir_mode='0644', $extr_dir_owner='root', $extr_dir_group='root', $extr_dir_seltype=undef) {

    package { 'unzip': ensure => present }

    $url = "https://gitlab.gnome.org/${namespace}/${project}/-/jobs/artifacts/${branch}/download?job=${job_name}"

    download_file { "${project}_artifacts":
        file_url  => "${url}",
        file_path => '/tmp/artifacts.zip',
    }

    file { "${extracted_path}":
        ensure  => directory,
        path    => "${extracted_path}",
        mode    => "${extr_dir_mode}",
        owner   => "${extr_dir_owner}",
        group   => "${extr_dir_group}",
        seltype => "${extr_dir_seltype}",
    }

    exec { "unzip_${project}_artifact":
        command     => "/usr/bin/unzip -o /tmp/artifacts.zip -d ${extracted_path}",
        user        => "${unzip_user_exec}",
        cwd         => "${extracted_path}",
        refreshonly => true,
        subscribe   => [ Download_file["${project}_artifacts"], File["${extracted_path}"] ]
    }
}
