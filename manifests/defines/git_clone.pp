define git_clone ($git_url, $branch, $prefix) {

          exec {"git_clone_${branch}": command => "/usr/bin/git clone -b ${branch} ${git_url} ${prefix}/${name}",
                  creates => "${prefix}/${name}/.git/HEAD",
                  require => [Package['git'], File["${prefix}/${name}"]]
          }

          file { "${prefix}/${name}":
                  owner   => 'apache',
                  group   => 'apache',
                  mode    => '0755',
                  seltype => 'httpd_sys_content_t',
                  require => Package['httpd'],
          }
}
