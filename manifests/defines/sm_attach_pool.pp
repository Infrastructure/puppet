define sm_attach_pool($pool_id) {
    exec { 'sm_attach_pool':
        command => "/usr/bin/subscription-manager attach --pool=${pool_id}",
        unless  => "/usr/bin/subscription-manager list --consumed | grep -q ${pool_id}",
    }
}
