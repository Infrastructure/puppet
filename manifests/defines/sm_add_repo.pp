define sm_add_repo($repo_name) {

    exec { "enable_${repo_name}":
        command => "/usr/bin/subscription-manager repos --enable=${repo_name}",
        unless  => "/usr/bin/subscription-manager repos --list-enabled | grep ${repo_name}",
    }
}
