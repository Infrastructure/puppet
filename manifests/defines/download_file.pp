define download_file($file_url=undef, $file_path=undef, $mode='0644', $owner='root', $group='root', $seltype=undef) {

    $_filename = split($file_path, '/')
    $filename = $_filename[-1]

    exec { "download_${name}":
        command => "/usr/bin/wget --quiet ${file_url} -O ${file_path} && md5sum ${file_path} > /tmp/${filename}.md5sum",
        unless  => "/usr/bin/wget --quiet ${file_url} -O /tmp/${filename} && /usr/bin/md5sum -c --status /tmp/${filename}.md5sum"
    }

    file { "${name}":
        path    => "${file_path}",
        mode    => $mode,
        owner   => $owner,
        group   => $group,
        seltype => $seltype,
        require => Exec["download_${name}"],
    }
}
