if versioncmp($::puppetversion,'3.6.1') >= 0 {
  Package {
    allow_virtual => false,
  }
}

import 'nodes/*.pp'
import 'defines/*.pp'
